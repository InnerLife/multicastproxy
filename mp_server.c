#include "mp_server.h"
#include "mps_worker.h"
#include "connection/connection_client.h"
#include "connection/connection_backend.h"

#include <stdio.h>
#include <errno.h>
#include <string.h>

#include <stdlib.h>
#include <unistd.h>

static int const THREAD_SLEEP_TIME = 1;

mp_server_t* init_mpserver(mp_server_t *mpserver, creader_t *reader){
	params_t *params = mpc_get_params(reader);
	if (params == NULL){
		LOG_ERROR(MPS_LOG_LEVEL, "Can not init mpserver. Error reading params.");
		return NULL;
	}
	memcpy(&mpserver->params, params, sizeof(mpserver->params));
	if (init_smeta(&mpserver->smeta, reader) == NULL){
		LOG_ERROR(MPS_LOG_LEVEL, "Can not init mpserver. Error init server meta.");
		return NULL;
	}
	mpserver->stopped = false;
	LOG_DEBUG(MPS_LOG_LEVEL, "mpserver initialized. %p", mpserver);
	return mpserver;
}

mp_server_t* alloc_mpserver(creader_t *reader){
	mp_server_t *mpserver = malloc(sizeof(mp_server_t));
	if (mpserver == NULL){
		LOG_ERROR(MPS_LOG_LEVEL, "Error allocating mpserver.");
		return NULL;
	}
	LOG_DEBUG(MPS_LOG_LEVEL, "mpserver allocated. %p", mpserver);
	if (init_mpserver(mpserver, reader) == NULL){
		return NULL;
	}
	return mpserver;
}

void free_mpserver_data(mp_server_t *mpserver){
	free_smeta_data(&mpserver->smeta);
	LOG_DEBUG(MPS_LOG_LEVEL, "mpserver data freed. %p", mpserver);
}

void free_mpserver(mp_server_t *mpserver){
	free_mpserver_data(mpserver);
	free(mpserver);
	LOG_DEBUG(MPS_LOG_LEVEL, "mpserver freed. %p", mpserver);
}

void* mps_worker_thread(void *mpserver_p){
	mp_server_t *mpserver = mpserver_p;
	
	mps_worker_t *worker = alloc_mps_worker(
		mpserver->params.buffer_size,
		mpserver->params.max_buffers,
		mpserver->params.type,
		mpserver->params.family,
		mpserver->params.port,
		mpserver->params.backlog,
		&mpserver->smeta,
		mpserver->params.clients_prealloc / mpserver->params.workers,
		mpserver->params.epoll_max_events,
		mpserver->params.epoll_timeout,
		mpserver->params.backend_idle_max,
		mpserver->params.client_idle_max,
		mpserver->params.idle_cleaner_sleep
	);
	if (worker == NULL){
		LOG_ERROR(MPS_LOG_LEVEL, "Can not start worker.");
		mpserver->stopped = true;
		return NULL;
	}	
	LOG_INFO(MPS_LOG_LEVEL, "Worker started.");

	while(!mpserver->stopped){
		if (mps_worker_poll(worker) == NULL){
			mpserver->stopped = true;
			LOG_ERROR(MPS_LOG_LEVEL, "Worker stopped due to server error.");
			break;
		}
	}
	
	free_mps_worker(worker);
	LOG_INFO(MPS_LOG_LEVEL, "Worker stopped.");
	
	return NULL;
}

void* mps_reload_backends_thread(void *mpserver_p){
	mp_server_t *mpserver = mpserver_p;
	time_t cur_time = time(0);
	time_t last_reload = cur_time;
	while(!mpserver->stopped){
		sleep(THREAD_SLEEP_TIME);
		cur_time = time(0);
		if(last_reload + mpserver->params.backends_reload_sleep < cur_time){
			smeta_load_backends(&mpserver->smeta);
			last_reload = cur_time;
		}
	}
	return NULL;
}
