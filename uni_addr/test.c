#include "uni_addr.h"

#include "uni_addr.c"

int main(){
	uni_addr_t addr1;
	uni_addr_t addr2;
	
	memset(&addr1, 0, sizeof(addr1));
	memset(&addr2, 0, sizeof(addr2));
	
	uaddr_fill("ya.ru", &addr1, SOCK_STREAM);
	uaddr_fill("google.com", &addr2, SOCK_STREAM);
	
	printf("%s\n", uaddr_readable(&addr1));
	printf("%s\n", uaddr_readable(&addr2));
	
	printf("%d\n", uaddr_cmp(&addr1, &addr2, false));
	printf("%d\n", uaddr_cmp(&addr1, &addr1, true));
	
	return 0;
}