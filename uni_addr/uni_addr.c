#include "uni_addr.h"

#include <sys/socket.h>
#include <sys/types.h>
#include <netdb.h>
#include <stdlib.h>
#include <threads.h>

#define ADDR_UNDEFINED "addr_undefined"
#define MAX_PORT_LEN 6 //5 digits + null terminator 
#define MAX_ADDR_LEN (INET6_ADDRSTRLEN + (MAX_PORT_LEN - 1) + 1)//1 - ":", null terminator is included to INET6_ADDRSTRLEN;

uni_addr_t* uaddr_fill(char *host, uni_addr_t *addr, int type){
	struct addrinfo hints;
	memset(&hints, 0, sizeof (hints));
	hints.ai_family = AF_UNSPEC;
	hints.ai_socktype = type;
	
	struct addrinfo *info;
	if (getaddrinfo(host, NULL, &hints, &info) < 0){
		LOG_INFO(UADDR_LOG_LEVEL, "Error getting addr info for %s. Type: %d.", host, type);
		return NULL;
	}

	do {
		memcpy(addr, info->ai_addr, info->ai_addrlen);
		//IPv4 is preferable
		if (info->ai_family == AF_INET){
			break;
		}
		info = info->ai_next;
	} while(info != NULL);

	freeaddrinfo(info);
	return addr;
}

static char* uaddr_readable_b(uni_addr_t *addr, char *readable){
	//char *p_readable = readable;
	short family = addr->sin.sin_family;
	void *sin_addr = NULL;
	switch(family){
		case AF_INET:
			sin_addr = (void*)&(addr->sin.sin_addr);
			break;
		case AF_INET6:
			sin_addr = (void*)&(addr->sin6.sin6_addr);
			break;
		default:
			strcpy(readable, ADDR_UNDEFINED);
			return readable;
	}
	if (inet_ntop(family, sin_addr, readable, INET6_ADDRSTRLEN) == NULL){
		strcpy(readable, ADDR_UNDEFINED);
		return readable;
	}
	//sprintf(readable, "%s:%d", readable, ntohs(addr->sin.sin_port));
	static char port[MAX_PORT_LEN];
	sprintf(port, "%d", ntohs(addr->sin.sin_port));
	strcat(readable, ":");
	strcat(readable, port);
	return readable;
}

static thread_local char readable1[MAX_ADDR_LEN];
static thread_local char readable2[MAX_ADDR_LEN];
static thread_local bool readable_num = false;
char* uaddr_readable(uni_addr_t *addr){
	//a hack
	//uaddr_readable can be called twice per one printf, so we need two indipedent char buffers
	if (readable_num){
		readable_num = false;
		return uaddr_readable_b(addr, readable1);
	} else {
		readable_num = true;
		return uaddr_readable_b(addr, readable2);
	}
}

bool uaddr_cmp(uni_addr_t *addr1, uni_addr_t *addr2, bool with_port){
	//not equal in case of different family or port number 
	if ((addr1->sin.sin_family != addr2->sin.sin_family) ||
		(with_port && addr1->sin.sin_port != addr2->sin.sin_port)){
		return false;
	}	
	//equal if addrs are the same
	if ((addr1->sin.sin_family == AF_INET && addr1->sin.sin_addr.s_addr == addr2->sin.sin_addr.s_addr) ||
		(addr1->sin.sin_family == AF_INET6 && memcmp(addr1->sin6.sin6_addr.s6_addr, addr2->sin6.sin6_addr.s6_addr, sizeof(addr1->sin6.sin6_addr.s6_addr)) == 0)){
		return true;
	}
	return false;
}