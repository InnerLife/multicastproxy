/** @file server_meta.h
 *  @brief IP address structure and a set of methods to deal with it in unified way.
 */
#ifndef H_UADDR
#define H_UADDR

#include "../log/log.h"
#include <stdbool.h>
#include <arpa/inet.h>

/**
 * @brief Uaddr union makes it possible to work with both IPv4 and IPv6 addr structures.
 */
typedef union uni_addr{
    struct sockaddr_in sin; /**< IPv4 struct */
    struct sockaddr_in6 sin6; /**< IPv6 struct */
} uni_addr_t;

/**
 * @brief Initialize uni_addr from given host name and socket type
 * 
 * @param host ip or hostname
 * @param addr addr to fill
 * @param type socket type(SOCK_DGRAM, SOCK_STREAM, ...)
 * @return addr if success, otherwise - NULL
 */
uni_addr_t* uaddr_fill(char *host, uni_addr_t *addr, int type);
/**
 * @brief Get a human-readable string from uni_addr
 * 
 * @param addr addr
 * @return human-readable string 
 */
char* uaddr_readable(uni_addr_t *addr);
/**
 * @brief Initialize uni_addr from given host name and socket type
 * 
 * @param addr addr1
 * @param addr addr2
 * @param with_port compare with port number
 * @return true if addresses are equal
 */
bool uaddr_cmp(uni_addr_t *addr1, uni_addr_t *addr2, bool with_port);

#endif

#ifndef UADDR_LOG_LEVEL
#define UADDR_LOG_LEVEL C_LOG_DEFAULT
#endif