#include "connection.h"

#include "connection.c"
#include "../uni_addr/uni_addr.c"
#include "../linked_list/linked_list.c"

int main(){

	int buffer_size = 1024;
	CONN_T(buffer_size) *conn = alloc_conn(CONN_TYPE_SERVER, buffer_size, time(0));
	memset(&conn->addr, 0, sizeof(conn->addr));
	conn->addr.sin.sin_family = AF_INET;	
	conn->addr.sin.sin_port = 1195;
	/*
	printf("%d %d %d %d %d %d %d %d\n", 
		conn->fd, 
		conn->last_active, 
		sizeof(conn->recv_buffers_prealloc), conn->recv_buffers.data_size, conn->recv_buffers.prealloc_size, 
		sizeof(conn->send_buffers_prealloc), conn->send_buffers.data_size, conn->send_buffers.prealloc_size
	);
	*/
	for (int i = 0; i < 10; i++){
		CONN_BUF_T(buffer_size) *buf = llist_take(&conn->recv_buffers);
		buf->meta.time_received = time(0);
		buf->meta.bytes_received = i;
		buf->meta.bytes_sended = i;
		strncpy(buf->data, "123", buffer_size);
		printf("%ld %ld %ld\n", buf->meta.time_received, buf->meta.bytes_received, buf->meta.bytes_sended);
	}
	uni_addr_t addr;
	uaddr_fill("127.0.0.1", &addr, SOCK_DGRAM);
	addr.sin.sin_port = htons(1194);
	addr.sin.sin_family = AF_INET;	
	printf("%s %d\n", uaddr_readable(&addr), addr.sin.sin_family);
	
	conn_connect(conn, SOCK_DGRAM, &addr);
	free_conn(conn);
	
	return 0;
}