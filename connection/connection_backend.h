/** @file connection_backend.h
 *  @brief Backend connection. Sending and receiving of data. Automatically adds and removes parent client connection from backend.
 */

#ifndef H_CONN_B
#define H_CONN_B

#include "../log/log.h"
#include "../backend/backend.h"
#include "connection.h"
#include "connection_server.h"
#include "connection_client.h"
#include <stdbool.h>

/**
 * @brief Build backend connection struct using buffer size.
 */
#define CONN_BACKEND_T(buff_size)\
	struct {\
		CONN_T(buff_size) base;\
		CONN_CLIENT_T(buff_size) *conn_client;\
		backend_t *backend;\
		pthread_mutex_t check_lock;\
	}
/**
 * @brief Build backend connection struct using connection ptr itself.
 */
#define CONN_BACKEND_TT(conn_ptr) CONN_BACKEND_T( CONN_BUFFER_SIZE(conn_ptr) )

/**
 * @brief Initialize already allocated backend connection
 * 
 * @param conn connection
 * @param conn_client parent client connection
 * @param cur_time current time to fill last_active
 * @return connection if success, otherwise - NULL
 */
void init_conn_backend(void *conn, backend_t *backend, void *conn_client, time_t cur_time);
/**
 * @brief Allocate and initialize backend connection
 * 
 * @param conn connection
 * @param conn_client parent client connection
 * @param cur_time current time to fill last_active
 * @return connection if success, otherwise - NULL
 */
void* alloc_conn_backend(backend_t *backend, void *conn_client, time_t cur_time);
/**
 * @brief Free backend connection internal data structures
 * 
 * @param conn connection
 */
void free_conn_backend_data(void *conn);
/**
 * @brief Free backend connection internal data structures and connection itself
 * 
 * @param conn connection
 */
void free_conn_backend(void *conn);

/**
 * @brief Close backend connection
 * 
 * @param conn connection
 */
void conn_close_backend(void *conn);
/**
 * @brief Connect backend connection to backend
 * 
 * @param conn connection
 * @return connection if success, NULL - connection error
 */
void* conn_backend_connect(void *conn);
/**
 * @brief Receive data from backend connection
 * 
 * @param conn connection
 * @param cur_time current time to update last_active
 * @param remove_client true, if need to remove parent client connection(approving error)
 * @param moved true, if buffers was moved from backends to client during recv call; can be true even if NULL is returning
 * @return connection if success, NULL - connection error
 */
void* conn_backend_recv(void *conn, time_t cur_time, bool *remove_client, bool *moved);
/**
 * @brief Send data to backend connection
 * 
 * @param conn connection
 * @param cur_time current time to update last_active
 * @param remove_client true, if need to remove parent client connection(approving error)
 * @param moved true, if buffers was moved from backends to client during recv call; can be true even if NULL is returning
 * @return connection if success, NULL - connection error
 */
void* conn_backend_send(void *conn, time_t cur_time, bool *remove_client, bool *moved);

#endif

#ifndef CONN_B_LOG_LEVEL
#define CONN_B_LOG_LEVEL C_LOG_DEFAULT
#endif