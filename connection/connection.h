/** @file connection.h
 *  @brief Base connection structure for server, client and backend connections.
 */
#ifndef H_CONN
#define H_CONN

#include "../log/log.h"
#include "../linked_list/linked_list.h"
#include "../uni_addr/uni_addr.h"
#include <time.h>
#include <pthread.h>

/**
 * @brief Preallocated buffers count. 1 is always need.
 */
#ifndef BUFFERS_PREALLOC
#define BUFFERS_PREALLOC 1
#endif

/**
 * @brief Connection types.
 */
#define CONN_TYPE_SERVER 0
#define CONN_TYPE_CLIENT 1
#define CONN_TYPE_BACKEND 2

/**
 * @brief Connection buffer metadata.
 */
#define CONN_BUF_META_T\
	struct {\
		time_t time_received;\
		ssize_t bytes_received;\
		ssize_t bytes_sended;\
	}
/**
 * @brief Connection buffer.
 */
#define CONN_BUF_T(buff_size)\
	struct {\
		CONN_BUF_META_T meta;\
		char data[buff_size];\
	}
/**
 * @brief Build connection struct using buffer size.
 */
#define CONN_T(buff_size)\
	struct {\
		uint8_t conn_type;\
		uni_addr_t addr;\
		int fd;\
		time_t last_active;\
		void *approved;\
		llist_t recv_buffers;\
		char recv_buffers_prealloc[LL_PREALLOC_SIZE(sizeof(CONN_BUF_T(buff_size)), BUFFERS_PREALLOC)];\
		llist_t send_buffers;\
		char send_buffers_prealloc[LL_PREALLOC_SIZE(sizeof(CONN_BUF_T(buff_size)), BUFFERS_PREALLOC)];\
	}
/**
 * @brief Get buffer size from connection instance ptr.
 */
#define CONN_BUFFER_SIZE(conn_ptr) ( ((CONN_T(1)*)conn_ptr)->recv_buffers.data_size - sizeof(CONN_BUF_META_T) )
/**
 * @brief Build connection struct using initialized connection ptr itself.
 */
#define CONN_TT(conn_ptr) CONN_T(CONN_BUFFER_SIZE(conn_ptr))

/**
 * @brief Initialize already allocated connection
 * 
 * @param conn connection
 * @param conn_type connection type, one of CONN_TYPE_*
 * @param buffer_size connection buffer size
 * @param cur_time current time to fill last_active
 * @return connection if success, otherwise - NULL
 */
void init_conn(void *conn, uint8_t conn_type, uint32_t buffer_size, time_t cur_time);
/**
 * @brief Allocate and initialize connection
 * 
 * @param conn connection
 * @param conn_type connection type, one of CONN_TYPE_*
 * @param buffer_size connection buffer size
 * @param cur_time current time to fill last_active
 * @return connection if success, otherwise - NULL
 */
void* alloc_conn(uint8_t conn_type, uint32_t buffer_size, time_t cur_time);
/**
 * @brief Free connection data internal structures
 * 
 * @param conn connection
 */
void free_conn_data(void *conn);
/**
 * @brief Free connection data internal structures and connection ptr itself
 * 
 * @param conn connection
 */
void free_conn(void *conn);

/**
 * @brief Close connection socket if it wan't closed already
 * 
 * @param conn connection
 */
void conn_close(void *conn);
/**
 * @brief Open connection (create and bind)
 * 
 * @param conn connection
 * @param type connection type (SOCK_DGRAM, SOCK_STREAM, ...)
 * @return connection if success, otherwise - NULL
 */
int conn_open(void *conn, int type);
/**
 * @brief Accept new connection
 * 
 * @param conn connection
 * @param type connection type (SOCK_DGRAM, SOCK_STREAM, ...)
 * @param addr addr of new connection
 * @return 0 - no connection to accept, -1 - conn error, otherwise - new connection fd
 */
int conn_accept(void *conn, int type, uni_addr_t *addr);
/**
 * @brief Connect to a given addr
 * 
 * @param conn connection
 * @param type connection type (SOCK_DGRAM, SOCK_STREAM, ...)
 * @param addr addr connect to
 * @return connection if success, otherwise - NULL
 */
void* conn_connect(void *conn, int type, uni_addr_t *addr);

/**
 * @brief Recieve data from connection.
 * 
 * @param conn connection
 * @param max_buffers max buffers(send+recv) per connection
 * @param cur_time current time to update last_active
 * @param new_buf ptr to new buffer
 * @return conn if max_buffers exceeded or can't allocate new buffer or no data to read, NULL - connection error
 */
void* conn_recv(void *conn, uint32_t max_buffers, time_t cur_time, void **new_buf);
/**
 * @brief Send data to connection.
 * 
 * @param conn connection
 * @param cur_time current time to update last_active
 * @return conn if no data to send or sended successfully, NULL - connection error
 */
void* conn_send(void *conn, time_t cur_time);
/**
 * @brief Copy buffer to connection.
 * 
 * @param conn_dst destination connection
 * @param src source buffer
 * @param max_buffers max buffers(send+recv) per connection
 * @return conn if copied successfully, NULL - allocation error or max_buffers exceeded
 */
void* conn_copy_buffer(void* conn_dst, void* src, uint32_t max_buffers);

#endif

#ifndef CONN_LOG_LEVEL
#define CONN_LOG_LEVEL C_LOG_DEFAULT
#endif