#include "connection_client.h"
#include "connection_backend.h"

#include <stdlib.h>

void init_conn_client(void *conn, int fd, uni_addr_t *addr, void *conn_server, time_t cur_time){
	uint32_t buffer_size = CONN_BUFFER_SIZE(conn_server);
	CONN_CLIENT_T(buffer_size) *conn_client = conn;
	init_conn(conn_client, CONN_TYPE_CLIENT, buffer_size, cur_time);
	init_llist(&conn_client->backend_conns, sizeof(CONN_BACKEND_T(buffer_size)), 0);	
	memcpy(&conn_client->base.addr, addr, sizeof(conn_client->base.addr));
	conn_client->base.fd = fd;
	conn_client->conn_server = conn_server;
	conn_client->initialized = false;
	conn_client->closed_by_rule = 0;
}

void* alloc_conn_client(int fd, uni_addr_t *addr, void *conn_server, time_t cur_time){
	void *conn = malloc(sizeof(CONN_CLIENT_T(CONN_BUFFER_SIZE(conn_server))));
	if (conn == NULL){
		LOG_ERROR(CONN_C_LOG_LEVEL, "Error allocating client connection memory.");
		return NULL;
	}
	init_conn_client(conn, fd, addr, conn_server, cur_time);
	return conn;
}

void free_conn_client_data(void *conn){
	CONN_CLIENT_TT(conn) *conn_client = conn;
	free_conn_data(conn_client);
	llist_iterator_t iter;
	for (void *conn_backend = llist_iter_init(&iter, &conn_client->backend_conns); conn_backend != NULL; conn_backend = llist_iter_next(&iter)){
		free_conn_backend_data(conn_backend);
	}
	free_llist_data(&conn_client->backend_conns);
}

void free_conn_client(void *conn){
	free_conn_client_data(conn);
	free(conn);
}

void conn_client_remove_backend(void *conn, void *conn_backend_p){
	CONN_CLIENT_TT(conn) *conn_client = conn;
	CONN_BACKEND_TT(conn_backend_p) *conn_backend = conn_backend_p;
	LOG_INFO(CONN_C_LOG_LEVEL, "Removing backend connection... (%d) %p %d %s %p %d %s", conn_client->backend_conns.size, conn_client, conn_client->base.fd, uaddr_readable(&conn_client->base.addr), 
		conn_backend, conn_backend->base.fd, uaddr_readable(&conn_backend->base.addr));
	free_conn_backend_data(conn_backend);
	llist_release(&conn_client->backend_conns, conn_backend);
	if (conn_backend == conn_client->base.approved){
		conn_client->base.approved = NULL;
	}
}

void conn_close_client(void *conn){
	CONN_CLIENT_TT(conn) *conn_client = conn;
	if (conn_client->base.fd == -1){
		return;
	}
	LOG_INFO(CONN_C_LOG_LEVEL, "Closing client connection... %p %d %s", conn_client, conn_client->base.fd, uaddr_readable(&conn_client->base.addr));
	conn_close(conn_client);
	llist_iterator_t iter;
	for (void *conn_backend = llist_iter_init(&iter, &conn_client->backend_conns); conn_backend != NULL; conn_backend = llist_iter_next(&iter)){
		conn_close_backend(conn_backend);
	}
}
			
static bool move_buffer_to_backends(void *conn, time_t cur_time){
	uint32_t buffer_size = CONN_BUFFER_SIZE(conn);
	CONN_CLIENT_T(buffer_size) *conn_client = conn;
	void *buf_from = llist_get_first(&conn_client->base.recv_buffers);
	if (buf_from == NULL){
		return false;
	}	
	llist_iterator_t iter;
	for (CONN_BACKEND_T(buffer_size) *conn_backend = llist_iter_init(&iter, &conn_client->backend_conns); conn_backend != NULL; conn_backend = llist_iter_next(&iter)){
		if (conn_backend->base.fd == -1){
			continue;
		}
		if (conn_copy_buffer(conn_backend, buf_from, conn_client->conn_server->max_buffers) == NULL){
			LOG_DEBUG(CONN_C_LOG_LEVEL, "Can not allocate backend buffer. Reverting... %p %d %s %d %s", 
				conn_client, conn_client->base.fd, uaddr_readable(&conn_client->base.addr), conn_backend->base.fd, uaddr_readable(&conn_backend->base.addr));
			for (CONN_BACKEND_T(buffer_size) *conn_backend2 = llist_iter_init(&iter, &conn_client->backend_conns); conn_backend2 != NULL; conn_backend2 = llist_iter_next(&iter)){
				if ((void*)conn_backend == (void*)conn_backend2){
					break;
				}
				llist_release(&conn_backend2->base.send_buffers, llist_get_last(&conn_backend2->base.send_buffers));
			}
			return false;
		}
	}
	llist_release(&conn_client->base.recv_buffers, buf_from);
	return true;
}

static bool buffers_to_backends(void *conn, time_t cur_time){
	CONN_CLIENT_TT(conn) *conn_client = conn;
	bool moved = false;
	LOG_DEBUG(CONN_C_LOG_LEVEL, "Moving buffers from client... %p %d %s", conn_client, conn_client->base.fd, uaddr_readable(&conn_client->base.addr));
	while (move_buffer_to_backends(conn_client, cur_time)){
		LOG_DEBUG(CONN_C_LOG_LEVEL, "One buffer moved from client. %p %d %s", conn_client, conn_client->base.fd, uaddr_readable(&conn_client->base.addr));
		moved = true;
	}	
	return moved;
}

static void* approve_by_cache(server_meta_t *smeta, void* conn, void *buffer){
	uint32_t buffer_size = CONN_BUFFER_SIZE(conn);
	CONN_CLIENT_T(buffer_size) *conn_client = conn;
	backend_t *backend = smeta_get_cache(smeta, buffer);
	if (backend == NULL){
		return conn_client;
	}
	void *conn_approved = NULL;
	llist_iterator_t iter;
	for (CONN_BACKEND_T(buffer_size) *conn_backend = llist_iter_init(&iter, &conn_client->backend_conns); conn_backend != NULL; conn_backend = llist_iter_next(&iter)){
		if (conn_backend->backend == backend){
			conn_approved = conn_backend;
			break;
		}
	}
	if (conn_approved == NULL){
		return conn_client;
	}
	LOG_DEBUG(CONN_C_LOG_LEVEL, "Trying to approve connection using cache. %p %d %s", conn_client, conn_client->base.fd, uaddr_readable(&conn_client->base.addr));
	if (be_approve_conn(backend, conn_client, conn_approved) == NULL){
		return NULL;
	}
	for (void *conn_backend = llist_iter_init(&iter, &conn_client->backend_conns); conn_backend != NULL; conn_backend = llist_iter_next(&iter)){
		if (conn_backend != conn_approved){
			conn_close_backend(conn_backend);
		}
	}
	return conn_client;
}

void* conn_client_recv(void *conn, time_t cur_time, bool *initialized, bool *moved){
	CONN_CLIENT_TT(conn) *conn_client = conn;
	void *new_buf = NULL;
	if (conn_recv(conn_client, conn_client->conn_server->max_buffers, cur_time, &new_buf) == NULL){
		return NULL;
	}
	if (new_buf != NULL){
		if (!conn_client->initialized){
			if (smeta_init_backends(conn_client->conn_server->smeta, conn_client, new_buf, cur_time) == NULL){
				return NULL;
			}
			conn_client->initialized = true;
			*initialized = true;
		} else if (conn_client->base.approved == NULL) {
			char *rule = smeta_close_client(conn_client->conn_server->smeta, new_buf);
			if (rule != NULL){
				return NULL;
			}
			if (approve_by_cache(conn_client->conn_server->smeta, conn_client, new_buf) == NULL){
				return NULL;
			}
		}
	}
	*moved = buffers_to_backends(conn_client, cur_time);//recv_buffers can be full, so conn_recv may not create new buffer; need to move buffers in any way
	return conn_client;
}

void* conn_client_send(void *conn, time_t cur_time){
	if (conn_send(conn, cur_time) == NULL){
		return NULL;
	}
	return conn;
}

static bool move_buffer_from_approved(void *conn){
	uint32_t buffer_size = CONN_BUFFER_SIZE(conn);
	CONN_CLIENT_T(buffer_size) *conn_client = conn;
	CONN_BACKEND_T(buffer_size) *conn_backend = conn_client->base.approved;
	CONN_BUF_T(buffer_size) *buf_from = llist_get_first(&conn_backend->base.recv_buffers);
	if (buf_from == NULL){
		return false;
	}
	if (conn_copy_buffer(conn_client, buf_from, conn_client->conn_server->max_buffers) == NULL){
		LOG_DEBUG(CONN_C_LOG_LEVEL, "Can not allocate approved client buffer. Reverting... %p %d %s %d %s", 
				conn_client, conn_client->base.fd, uaddr_readable(&conn_client->base.addr), conn_backend->base.fd, uaddr_readable(&conn_backend->base.addr));
		return false;
	}
	llist_release(&conn_backend->base.recv_buffers, buf_from);
	return true;
}

static bool move_buffer_from_unapproved(void *conn){
	uint32_t buffer_size = CONN_BUFFER_SIZE(conn);
	CONN_CLIENT_T(buffer_size) *conn_client = conn;
	
	bool moved = false;
	llist_iterator_t iter;
	for (CONN_BACKEND_T(buffer_size) *conn_backend = llist_iter_init(&iter, &conn_client->backend_conns); conn_backend != NULL; conn_backend = llist_iter_next(&iter)){
		if (conn_backend->base.fd == -1){
			continue;
		}
		void *buf_from = llist_get_first(&conn_backend->base.recv_buffers);
		if (!moved){			
			if (conn_copy_buffer(conn_client, buf_from, conn_client->conn_server->max_buffers) == NULL){
				LOG_DEBUG(CONN_C_LOG_LEVEL, "Can not allocate approved client buffer. Reverting... %p %d %s %d %s", 
						conn_client, conn_client->base.fd, uaddr_readable(&conn_client->base.addr), conn_backend->base.fd, uaddr_readable(&conn_backend->base.addr));
				return false;
			}
			moved = true;
		}
		llist_release(&conn_backend->base.recv_buffers, buf_from);
	}
	return true;
}

static bool move_buffer_from_backends(void *conn, bool *remove_client){
	uint32_t buffer_size = CONN_BUFFER_SIZE(conn);
	CONN_CLIENT_T(buffer_size) *conn_client = conn;
	
	uint32_t all_count = 0;
	uint32_t ready_count = 0;
	void *conn_ready = NULL;
	llist_iterator_t iter;
	for (CONN_BACKEND_T(buffer_size) *conn_backend = llist_iter_init(&iter, &conn_client->backend_conns); conn_backend != NULL; conn_backend = llist_iter_next(&iter)){
		if (conn_backend->base.fd == -1){
			continue;
		}
		all_count++;
		if (conn_backend->base.approved != NULL){
			return move_buffer_from_approved(conn_client);
		}
		if (conn_backend->base.recv_buffers.size > 0){
			ready_count++;
		}
		if (ready_count < all_count && conn_client->closed_by_rule == 0){
			return false;
		}
		if (ready_count < all_count - 1 && conn_client->closed_by_rule > 0){
			return false;
		}
		conn_ready = conn_backend;
	}
	
	if (all_count == 1 && ready_count == 1 && conn_client->closed_by_rule > 0){
		if (conn_client_approve(conn_client, conn_ready) == NULL){
			*remove_client = true;
			return false;
		}
		return move_buffer_from_approved(conn_client);
	}
	if (all_count != ready_count){
		return false;
	}
	return move_buffer_from_unapproved(conn_client);
}

static bool move_buffer(void *conn, bool *remove_client){
	CONN_CLIENT_TT(conn) *conn_client = conn;
	if (conn_client->base.approved != NULL){
		return move_buffer_from_approved(conn_client);
	}
	return move_buffer_from_backends(conn, remove_client);
}

//can be called concurrently from backend conn threads
bool conn_client_move_buffers(void *conn, bool *remove_client){
	CONN_CLIENT_TT(conn) *conn_client = conn;
	bool moved = false;
	LOG_DEBUG(CONN_C_LOG_LEVEL, "Moving buffers to client... %p %d %s", conn_client, conn_client->base.fd, uaddr_readable(&conn_client->base.addr));
	while (move_buffer(conn_client, remove_client)){
		LOG_DEBUG(CONN_C_LOG_LEVEL, "One buffer moved to client. %p %d %s", conn_client, conn_client->base.fd, uaddr_readable(&conn_client->base.addr));
		moved = true;
	}	
	return moved;
}

void* conn_client_approve(void *conn, void *conn_approved){
	CONN_CLIENT_TT(conn) *conn_client = conn;
	CONN_BACKEND_TT(conn) *conn_backend_approved = conn_approved;
	if (conn_client->base.approved != NULL){
		return conn_client;
	}
	if (be_approve_conn(conn_backend_approved->backend, conn_client, conn_backend_approved) == NULL){
		return NULL;
	}
	llist_iterator_t iter;
	for (void *conn_backend = llist_iter_init(&iter, &conn_client->backend_conns); conn_backend != NULL; conn_backend = llist_iter_next(&iter)){
		if (conn_backend != conn_backend_approved){
			conn_close_backend(conn_backend);
		}
	}
	if (conn_client->base.recv_buffers.size == 0){
		//one preallocated buffer is not zeroed, so we can reuse last released buffer
		void *buf = llist_take(&conn_client->base.recv_buffers);
		be_add_cache(conn_backend_approved->backend, buf);
		llist_release(&conn_client->base.recv_buffers, buf);
	}
	return conn_client;
}