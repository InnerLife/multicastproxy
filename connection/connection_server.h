/** @file connection_server.h
 *  @brief Server connection. Listen and accept new client connections.
 */
#ifndef H_CONN_S
#define H_CONN_S

#include "../log/log.h"
#include "connection.h"
#include "../server_meta/server_meta.h"
#include <stdint.h>

/**
 * @brief Build server connection struct using buffer size.
 */
#define CONN_SERVER_T(buff_size)\
	struct {\
		CONN_T(buff_size) base;\
		uint32_t max_buffers;\
		int type;\
		int backlog;\
		server_meta_t *smeta;\
		pthread_mutex_t client_conns_lock;\
		llist_t *client_conns;\
	}
/**
 * @brief Build server connection struct using connection ptr itself.
 */
#define CONN_SERVER_TT(conn_ptr) CONN_SERVER_T( CONN_BUFFER_SIZE(conn_ptr) )

/**
 * @brief Initialize already allocated server connection
 * 
 * @param conn connection
 * @param buffer_size connection buffer size
 * @param max_buffers max buffers(send+recv) per connection
 * @param type connection type (SOCK_DGRAM, SOCK_STREAM, ...)
 * @param family connection family (AF_INET, AF_INET6, ...)
 * @param port port listen to
 * @param backlog tcp listen backlog
 * @param smeta server meta data
 * @param clients_count count of client to preallocate
 * @return connection if success, otherwise - NULL
 */
void* init_conn_server(void *conn, uint32_t buffer_size, uint32_t max_buffers, int type, int family, short port, int backlog, server_meta_t *smeta, uint32_t clients_count);
/**
 * @brief Allocate and initialize server connection
 * 
 * @param conn connection
 * @param buffer_size connection buffer size
 * @param max_buffers max buffers(send+recv) per connection
 * @param type connection type (SOCK_DGRAM, SOCK_STREAM, ...)
 * @param family connection family (AF_INET, AF_INET6, ...)
 * @param port port listen to
 * @param backlog tcp listen backlog
 * @param smeta server meta data
 * @param clients_count count of client to preallocate
 * @return connection if success, otherwise - NULL
 */
void* alloc_conn_server(uint32_t buffer_size, uint32_t max_buffers, int type, int family, short port, int backlog, server_meta_t *smeta, uint32_t clients_count);

/**
 * @brief Remove client connection from server connection
 * 
 * @param conn server connection
 * @param conn_client_p client connection
 */
void conn_server_remove_client(void *conn, void *conn_client_p);
/**
 * @brief Free server connection internal data structures(including client connections)
 * 
 * @param conn server connection
 */
void free_conn_server_data(void *conn);
/**
 * @brief Free server connection internal data structures(including client connections) and connection ptr itself
 * 
 * @param conn server connection
 */
void free_conn_server(void *conn);
/**
 * @brief Close server connection and all client connections
 * 
 * @param conn server connection
 */
void conn_close_server(void *conn);

/**
 * @brief Open server connection and listen for clients
 * 
 * @param conn server connection
 * @return connection if success, otherwise - NULL
 */
void* conn_server_open(void *conn);
/**
 * @brief Accept new client connection
 * 
 * @param conn server connection
 * @param cur_time current time to initialize new client connection
 * @param new_client ptr to new initialized client connection
 * @return connection if success, NULL - server error
 */
void* conn_server_accept(void *conn, time_t cur_time, void **new_client);

#endif

#ifndef CONN_S_LOG_LEVEL
#define CONN_S_LOG_LEVEL C_LOG_DEFAULT
#endif