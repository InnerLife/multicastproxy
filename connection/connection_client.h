/** @file connection_client.h
 *  @brief Client connection. Sending and receiving of data. Initialization(through server meta) of new backend connections. Moving buffers to backend and from backends. Approving.
 */
#ifndef H_CONN_C
#define H_CONN_C

#include "../log/log.h"
#include "../uni_addr/uni_addr.h"
#include "connection.h"
#include "connection_server.h"
#include <stdint.h>
#include <stdbool.h>

/**
 * @brief Build client connection struct using buffer size.
 */
#define CONN_CLIENT_T(buff_size)\
	struct {\
		CONN_T(buff_size) base;\
		CONN_SERVER_T(buff_size) *conn_server;\
		bool initialized;\
		uint32_t closed_by_rule;\
		llist_t backend_conns;\
		pthread_mutex_t backend_conns_lock;\
	}
/**
 * @brief Build client connection struct using connection ptr itself.
 */
#define CONN_CLIENT_TT(conn_ptr) CONN_CLIENT_T( CONN_BUFFER_SIZE(conn_ptr) )

/**
 * @brief Initialize already allocated client connection
 * 
 * @param conn connection
 * @param fd client connection socket fd
 * @param addr client connection addr
 * @param conn_server parent server connection
 * @param cur_time current time to fill last_active
 * @return connection if success, otherwise - NULL
 */
void init_conn_client(void *conn, int fd, uni_addr_t *addr, void *conn_server, time_t cur_time);
/**
 * @brief Allocate and initialize client connection
 * 
 * @param conn connection
 * @param fd client connection socket fd
 * @param addr client connection addr
 * @param conn_server parent server connection
 * @param cur_time current time to fill last_active
 * @return connection if success, otherwise - NULL
 */
void* alloc_conn_client(int fd, uni_addr_t *addr, void *conn_server, time_t cur_time);

/**
 * @brief Free client connection internal data structures(including backend connections)
 * 
 * @param conn connection
 */
void free_conn_client_data(void *conn);
/**
 * @brief Free client connection internal data structures(including backend connections) and connection ptr itself
 * 
 * @param conn connection
 */
void free_conn_client(void *conn);
/**
 * @brief Close client connection and all backend connections
 * 
 * @param conn connection
 */
void conn_close_client(void *conn);

/**
 * @brief Remove backend connection from client connection
 * 
 * @param conn client connection
 * @param conn_backend_p backend connection
 */
void conn_client_remove_backend(void *conn, void *conn_backend_p);
/**
 * @brief Recv data from connection
 * 
 * @param conn client connection
 * @param cur_time current time to update last_active
 * @param initialized true, if backend connections was created during current recv loop
 * @param moved, true if new recv buffer was successfully moved to backend connections
 * @return connection if success, NULL - connection error
 */
void* conn_client_recv(void *conn, time_t cur_time, bool *initialized, bool *moved);
/**
 * @brief Recv data from connection
 * 
 * @param conn client connection
 * @param cur_time current time to update last_active
 * @return connection if success, NULL - connection error
 */
void* conn_client_send(void *conn, time_t cur_time);
/**
 * @brief Move buffers from backends to client. Called from backend.
 * 
 * @param conn client connection
 * @return true, if any of buffers was actually moved
 */
bool conn_client_move_buffers(void *conn, bool *remove_client);
/**
 * @brief Approve client connection. Called from backend.
 * 
 * @param conn client connection
 * @param conn_approved approved backend connection
 * @return connection if success, NULL - limits exceeded
 */
void* conn_client_approve(void *conn, void *conn_approved);

#endif

#ifndef CONN_C_LOG_LEVEL
#define CONN_C_LOG_LEVEL C_LOG_DEFAULT
#endif