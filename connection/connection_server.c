#include "connection_server.h"
#include "connection_client.h"

#include <stdlib.h>
#include <unistd.h>

void* init_conn_server(void *conn, uint32_t buffer_size, uint32_t max_buffers, int type, int family, short port, int backlog, server_meta_t *smeta, uint32_t clients_count){
	CONN_SERVER_T(buffer_size) *conn_server = conn;
	conn_server->client_conns = alloc_llist(sizeof(CONN_CLIENT_T(buffer_size)), clients_count);
	if (conn_server->client_conns == NULL){
		LOG_ERROR(CONN_LOG_LEVEL, "Error allocating server clients list.");
		return NULL;
	}
	init_conn(conn_server, CONN_TYPE_SERVER, buffer_size, time(0));
	memset(&conn_server->base.addr, 0, sizeof(conn_server->base.addr));
	conn_server->base.addr.sin.sin_family = family;
	conn_server->base.addr.sin.sin_port = htons(port);
	conn_server->base.fd = -1;
	conn_server->backlog = backlog;
	conn_server->smeta = smeta;
	conn_server->max_buffers = max_buffers;
	conn_server->type = type;
	return conn_server;
}

void* alloc_conn_server(uint32_t buffer_size, uint32_t max_buffers, int type, int family, short port, int backlog, server_meta_t *smeta, uint32_t clients_count){
	void *conn = malloc(sizeof(CONN_SERVER_T(buffer_size)));
	if (conn == NULL){
		LOG_ERROR(CONN_LOG_LEVEL, "Error allocating server connection memory.");
		return NULL;
	}
	if (init_conn_server(conn, buffer_size, max_buffers, type, family, port, backlog, smeta, clients_count) == NULL){
		free(conn);
		return NULL;
	}
	return conn;
}

void free_conn_server_data(void *conn){
	CONN_SERVER_TT(conn) *conn_server = conn;
	free_conn_data(conn_server);
	llist_iterator_t iter;
	for (void *conn_client = llist_iter_init(&iter, conn_server->client_conns); conn_client != NULL; conn_client = llist_iter_next(&iter)){
		free_conn_client_data(conn_client);
	}
	free_llist(conn_server->client_conns);
}

void free_conn_server(void *conn){
	free_conn_server_data(conn);
	free(conn);
}

void conn_server_remove_client(void *conn, void *conn_client_p){
	CONN_SERVER_TT(conn) *conn_server = conn;
	CONN_CLIENT_TT(conn_client_p) *conn_client = conn_client_p;
	LOG_INFO(CONN_LOG_LEVEL, "Removing client connection... (%d) %p %d %s %p %d %s", conn_server->client_conns->size, conn_server, conn_server->base.fd, uaddr_readable(&conn_server->base.addr), 
		conn_client, conn_client->base.fd, uaddr_readable(&conn_client->base.addr));
	free_conn_client_data(conn_client);
	llist_release(conn_server->client_conns, conn_client);
}

void conn_close_server(void *conn){
	CONN_SERVER_TT(conn) *conn_server = conn;
	LOG_INFO(CONN_LOG_LEVEL, "Closing server connection... %p %d %s", conn_server, conn_server->base.fd, uaddr_readable(&conn_server->base.addr));
	conn_close(conn_server);
	llist_iterator_t iter;
	for (void *conn_client = llist_iter_init(&iter, conn_server->client_conns); conn_client != NULL; conn_client = llist_iter_next(&iter)){
		conn_close_client(conn_client);
	}
}

void* conn_server_open(void *conn){
	CONN_SERVER_TT(conn) *conn_server = conn;
	if (conn_open(conn_server, conn_server->type) == -1){
		return NULL;
	}		
	if (conn_server->type == SOCK_STREAM){
		if (listen(conn_server->base.fd, conn_server->backlog) < 0){
			LOG_ERROR(CONN_S_LOG_LEVEL, "Error listening on socket.");
			close(conn_server->base.fd);
			return NULL;
		}
	}
	LOG_INFO(CONN_LOG_LEVEL, "Server connection opened. %p %d %s", conn_server, conn_server->base.fd, uaddr_readable(&conn_server->base.addr));
	return conn_server;
}
	
static void init_client(void *conn, time_t cur_time, void **new_client, uni_addr_t *addr, int fd){
	uint32_t buffer_size = CONN_BUFFER_SIZE(conn);
	CONN_SERVER_T(buffer_size) *conn_server = conn;
	CONN_CLIENT_T(buffer_size) *conn_client = llist_take(conn_server->client_conns);
	if (conn_client == NULL){
		LOG_INFO(CONN_S_LOG_LEVEL, "Can not allocate new client connection. %p %d %s %s", conn_server, conn_server->base.fd, uaddr_readable(&conn_server->base.addr), uaddr_readable(addr));
		return;
	}
	init_conn_client(conn_client, fd, addr, conn_server, cur_time);
	LOG_INFO(CONN_S_LOG_LEVEL, "New client accepted. (%d) %p %d %s %p %d %s", conn_server->client_conns->size, conn_server, conn_server->base.fd, uaddr_readable(&conn_server->base.addr),
		conn_client, conn_client->base.fd, uaddr_readable(&conn_client->base.addr));
	*new_client = conn_client;
}
	
void* conn_server_accept(void *conn, time_t cur_time, void **new_client){
	uint32_t buffer_size = CONN_BUFFER_SIZE(conn);
	CONN_SERVER_T(buffer_size) *conn_server = conn;
	uni_addr_t addr;
	int fd = conn_accept(conn_server, conn_server->type, &addr);
	if (fd == -1){
		return NULL;
	}
	if (fd == 0){
		return conn_server;
	}
	init_client(conn_server, cur_time, new_client, &addr, fd);
	return conn_server;
}