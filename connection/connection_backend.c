#include "connection_backend.h"

#include <stdlib.h>

void init_conn_backend(void *conn, backend_t *backend, void *conn_client, time_t cur_time){
	uint32_t buffer_size = CONN_BUFFER_SIZE(conn_client);
	CONN_BACKEND_T(buffer_size) *conn_backend = conn;
	init_conn(conn_backend, CONN_TYPE_BACKEND, buffer_size, cur_time);
	conn_backend->conn_client = conn_client;
	conn_backend->backend = backend;
	memset(&conn_backend->base.addr, 0, sizeof(conn_backend->base.addr));
}

void* alloc_conn_backend(backend_t *backend, void *conn_client, time_t cur_time){
	void *conn = malloc(sizeof(CONN_BACKEND_T(CONN_BUFFER_SIZE(conn_client))));
	if (conn == NULL){
		LOG_ERROR(CONN_B_LOG_LEVEL, "Error allocating client connection memory.");
		return NULL;
	}
	init_conn_backend(conn, backend, conn_client, cur_time);
	return conn;
}

void free_conn_backend_data(void *conn){
	CONN_BACKEND_TT(conn) *conn_backend = conn;
	free_conn_data(conn_backend);
}

void free_conn_backend(void *conn){
	free_conn_backend_data(conn);
	free(conn);
}

void conn_close_backend(void *conn){
	CONN_BACKEND_TT(conn) *conn_backend = conn;
	if (conn_backend->base.fd == -1){
		return;
	}
	LOG_INFO(CONN_LOG_LEVEL, "Closing backend connection... %p %d %s", conn_backend, conn_backend->base.fd, uaddr_readable(&conn_backend->base.addr));
	conn_close(conn_backend);
	be_remove_conn(conn_backend->backend, conn_backend->conn_client);
}

void* conn_backend_connect(void *conn){
	CONN_BACKEND_TT(conn) *conn_backend = conn;
	if (conn_connect(conn_backend, conn_backend->conn_client->conn_server->type, &conn_backend->backend->addr) == NULL){
		return NULL;
	}
	if (be_add_conn(conn_backend->backend, conn_backend->conn_client) == NULL){
		return NULL;
	}
	LOG_INFO(CONN_B_LOG_LEVEL, "Backend connected. %p %d %s %s", conn_backend, conn_backend->base.fd, uaddr_readable(&conn_backend->base.addr), uaddr_readable(&conn_backend->backend->addr));
	return conn_backend;
}

static void* check_buf(void *conn, void* buf, bool *approved){
	CONN_BACKEND_TT(conn) *conn_backend = conn;
	char *rule = smeta_approve(conn_backend->conn_client->conn_server->smeta, buf);
	if (rule != NULL){
		conn_backend->base.approved = conn_backend;
		*approved = true;
		LOG_INFO(CONN_B_LOG_LEVEL, "Backend connection approved by rule. %p %d %s %s", conn_backend, conn_backend->base.fd, rule, uaddr_readable(&conn_backend->base.addr));
	} else {
		rule = smeta_close(conn_backend->conn_client->conn_server->smeta, buf);
		if (rule != NULL){
			conn_backend->conn_client->closed_by_rule++;
			LOG_INFO(CONN_B_LOG_LEVEL, "Backend connection should be closed by rule. %p %d %s %s", conn_backend, conn_backend->base.fd, rule, uaddr_readable(&conn_backend->base.addr));
			return NULL;
		}
	}
	return conn_backend;
}

static void* approve(void *conn, void *buf, bool *remove_client){
	CONN_BACKEND_TT(conn) *conn_backend = conn;
	bool approved = false;
	if (conn_backend->base.approved == NULL){
		if(check_buf(conn_backend, buf, &approved) == NULL){
			return NULL;
		}
	}
	if (approved && conn_client_approve(conn_backend->conn_client, conn_backend) == NULL){
		*remove_client = true;
		return NULL;
	}
	return conn_backend;
}

//connection is non-blocking, need to get addr after it will be ready
static void get_addr(void *conn){
	CONN_BACKEND_TT(conn) *conn_backend = conn;
	if (conn_backend->base.addr.sin.sin_family == 0){
		socklen_t addr_size = sizeof(conn_backend->base.addr);
		if (getsockname(conn_backend->base.fd, (struct sockaddr*)&conn_backend->base.addr, &addr_size) < 0){
			LOG_INFO(CONN_B_LOG_LEVEL, "Can't get backend conn addr. %p %d %s %s", conn_backend, conn_backend->base.fd, uaddr_readable(&conn_backend->base.addr), uaddr_readable(&conn_backend->backend->addr));
		}
	}
}

void* conn_backend_recv(void *conn, time_t cur_time, bool *remove_client, bool *moved){
	CONN_BACKEND_TT(conn) *conn_backend = conn;
	get_addr(conn_backend);
	void *new_buf = NULL;
	void *res = conn_recv(conn_backend, conn_backend->conn_client->conn_server->max_buffers, cur_time, &new_buf);
	if (res != NULL && new_buf != NULL){
		res = approve(conn, new_buf, remove_client);
	}
	*moved = conn_client_move_buffers(conn_backend->conn_client, remove_client);
	return res;
}

void* conn_backend_send(void *conn, time_t cur_time, bool *remove_client, bool *moved){
	CONN_BACKEND_TT(conn) *conn_backend = conn;
	get_addr(conn_backend);
	if (conn_send(conn_backend, cur_time) == NULL){
		*moved = conn_client_move_buffers(conn_backend->conn_client, remove_client);
		return NULL;
	}
	return conn_backend;
}
