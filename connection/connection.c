#include "connection.h"

#include <stdlib.h>
#include <sys/socket.h>
#include <unistd.h>
#include <fcntl.h>
#include <netinet/tcp.h>

void init_conn(void *conn, uint8_t conn_type, uint32_t buffer_size, time_t cur_time){
	CONN_T(buffer_size) *conn_base = conn;
	conn_base->conn_type = conn_type;
	conn_base->last_active = cur_time;
	conn_base->approved = NULL;
	conn_base->fd = -1;
	init_llist(&conn_base->recv_buffers, sizeof(CONN_BUF_T(buffer_size)), BUFFERS_PREALLOC);
	init_llist(&conn_base->send_buffers, sizeof(CONN_BUF_T(buffer_size)), BUFFERS_PREALLOC);
	LOG_DEBUG(CONN_LOG_LEVEL, "Connection initialized. %p", conn);
}

void* alloc_conn(uint8_t conn_type, uint32_t buffer_size, time_t cur_time){
	void *conn = malloc(sizeof(CONN_T(buffer_size)));
	if (conn == NULL){
		LOG_ERROR(CONN_LOG_LEVEL, "Error allocating connection memory.");
		return NULL;
	}
	LOG_DEBUG(CONN_LOG_LEVEL, "Connection allocated. %p", conn);
	init_conn(conn, conn_type, buffer_size, cur_time);
	return conn;
}

void free_conn_data(void *conn){
	CONN_TT(conn) *conn_base = conn;
	free_llist_data(&conn_base->recv_buffers);
	free_llist_data(&conn_base->send_buffers);
	LOG_DEBUG(CONN_LOG_LEVEL, "Connection data freed. %p", conn);
}

void free_conn(void *conn){
	free_conn_data(conn);
	free(conn);
	LOG_DEBUG(CONN_LOG_LEVEL, "Connection freed. %p", conn);
}

void conn_close(void *conn){
	CONN_TT(conn) *conn_base = conn;
	if (conn_base->fd == -1){
		return;
	}
	if (conn_base->addr.sin.sin_family != SOCK_DGRAM) {
		shutdown(conn_base->fd, SHUT_RDWR);
	}
	close(conn_base->fd);
	LOG_DEBUG(CONN_LOG_LEVEL, "Connection closed. %p %d %s", conn_base, conn_base->fd, uaddr_readable(&conn_base->addr));
	conn_base->fd = -1;
}

int conn_open(void *conn, int type){
	CONN_TT(conn) *conn_base = conn;
	conn_base->fd = socket(conn_base->addr.sin.sin_family, type, 0);
	if (conn_base->fd < 0){
		LOG_ERROR(CONN_LOG_LEVEL, "Error creating socket. %p", conn_base);
		return -1;
	}
	if (fcntl(conn_base->fd, F_SETFL, O_NONBLOCK)){
		LOG_ERROR(CONN_LOG_LEVEL, "Socket can not be set to non-blocking mode. %p", conn_base);
		close(conn_base->fd);
		return -1;
	}
	if (setsockopt(conn_base->fd, SOL_SOCKET, SO_REUSEADDR, &(int){ 1 }, sizeof(int)) != 0){
		LOG_ERROR(CONN_LOG_LEVEL, "Can not set socket to REUSEADDR mode. %p", conn_base);
		close(conn_base->fd);
		return -1;
	}
	if (setsockopt(conn_base->fd, SOL_SOCKET, SO_REUSEPORT, &(int){ 1 }, sizeof(int)) != 0){
		LOG_ERROR(CONN_LOG_LEVEL, "Can not set socket to REUSEPORT mode. %p", conn_base);
		close(conn_base->fd);
		return -1;
	}
	if (conn_base->addr.sin.sin_family == AF_INET6){
		if (setsockopt(conn_base->fd, IPPROTO_IPV6, IPV6_V6ONLY, &(int){ 0 }, sizeof(int)) != 0){
			LOG_ERROR(CONN_LOG_LEVEL, "Can not set server socket to V6 and V4 both mode. %p", conn_base);
			close(conn_base->fd);
			return -1;
		}
	}
	if (bind(conn_base->fd, (struct sockaddr*)&conn_base->addr, sizeof(uni_addr_t)) < 0){
		LOG_ERROR(CONN_LOG_LEVEL, "Error binding server socket. %p", conn_base);
		close(conn_base->fd);
		return -1;
	}
	LOG_DEBUG(CONN_LOG_LEVEL, "Connection opened. %p %d %s", conn_base, conn_base->fd, uaddr_readable(&conn_base->addr));
	return conn_base->fd;
}

int conn_accept(void *conn, int type, uni_addr_t *addr){
	uint32_t buffer_size = CONN_BUFFER_SIZE(conn);
	CONN_T(buffer_size) *conn_base = conn;
	int fd;
	socklen_t addr_size = sizeof(*addr);
	if (type == SOCK_DGRAM){
		CONN_BUF_T(buffer_size) buf;
		buf.meta.bytes_received = recvfrom(conn_base->fd, buf.data, buffer_size, MSG_PEEK, (struct sockaddr*)addr, &addr_size);
		if (buf.meta.bytes_received < 0){
			if (errno != EWOULDBLOCK && errno != EAGAIN){
				return 0;
			}
			LOG_ERROR(CONN_LOG_LEVEL, "Can not accept new client. Connection hanged. %p %d %s [%d,%s]", conn_base, conn_base->fd, uaddr_readable(&conn_base->addr), errno, strerror(errno));
			return -1;
		} else if (buf.meta.bytes_received == 0) {
			LOG_ERROR(CONN_LOG_LEVEL, "Can not accept new client. Connection closed. %p %d %s", conn_base, conn_base->fd, uaddr_readable(&conn_base->addr));
			return -1;
		}
		fd = conn_base->fd;
		if (connect(fd, (struct sockaddr*)addr, addr_size) < 0){
			LOG_ERROR(CONN_LOG_LEVEL, "Can not connect old socket to addr. %p %d %s", conn_base, conn_base->fd, uaddr_readable(&conn_base->addr));
			return -1;
		}		
		if (conn_open(conn_base, type) < 0){
			LOG_ERROR(CONN_LOG_LEVEL, "Can not open new SOCK_DGRAM connection. %p %d %s", conn_base, conn_base->fd, uaddr_readable(&conn_base->addr));
			return -1;
		}
		LOG_INFO(CONN_LOG_LEVEL, "New conn fd was generated. %p %d->%d %s", conn_base, fd, conn_base->fd, uaddr_readable(&conn_base->addr));
	} else {
		fd = accept(conn_base->fd, (struct sockaddr*)addr, &addr_size);
		if (fd < 0){
			if (errno != EWOULDBLOCK && errno != EAGAIN){
				LOG_ERROR(CONN_LOG_LEVEL, "Connection hanged during accept. %p %d %s [%d,%s]", conn_base, conn_base->fd, uaddr_readable(&conn_base->addr), errno, strerror(errno));
				return -1;
			}
			return 0;
		}
		if (fcntl(fd, F_SETFL, O_NONBLOCK)){
			LOG_ERROR(CONN_LOG_LEVEL, "Can not set new client to non-blocking mode. %p %d %s", conn_base, conn_base->fd, uaddr_readable(&conn_base->addr));
			close(fd);
			return 0;
		}
	}
	LOG_INFO(CONN_LOG_LEVEL, "New connection accepted. %p %d %s %d %s", conn_base, conn_base->fd, uaddr_readable(&conn_base->addr), fd, uaddr_readable(addr));
	return fd;
}

void* conn_connect(void *conn, int type, uni_addr_t *addr){
	CONN_TT(conn) *conn_base = conn;
	conn_base->fd = socket(addr->sin.sin_family, type, 0);
	if (conn_base->fd < 0){
		LOG_ERROR(CONN_LOG_LEVEL, "Error creating socket while connecting to addr. %p %d %s", conn_base, conn_base->fd, uaddr_readable(addr));
		return NULL;
	}
	if (fcntl(conn_base->fd, F_SETFL, O_NONBLOCK)){
		LOG_ERROR(CONN_LOG_LEVEL, "Socket can not be set to non-blocking mode while connecting to addr. %p %d %s", conn_base, conn_base->fd, uaddr_readable(addr));
		close(conn_base->fd);
		return NULL;
	}
	if (connect(conn_base->fd, (struct sockaddr*)addr, sizeof(*addr)) < 0 && errno != EINPROGRESS){
		LOG_ERROR(CONN_LOG_LEVEL, "Can not connect to addr. %p %d %s [%d,%s]", conn_base, conn_base->fd, uaddr_readable(addr), errno, strerror(errno));
		close(conn_base->fd);
		return NULL;
	}
	/*
	//should be filled after first receive, moved to backend conn
	socklen_t addr_size = sizeof(conn_base->addr);
	if (getpeername(conn_base->fd, (struct sockaddr*)&conn_base->addr, &addr_size) < 0){
		LOG_ERROR(CONN_LOG_LEVEL, "Can not get peer name while connecting to addr. %p %d %s ", conn_base, conn_base->fd, uaddr_readable(addr));
		close(conn_base->fd);
		return NULL;
	}
	*/	
	
	setsockopt(conn_base->fd, IPPROTO_TCP, TCP_KEEPCNT, &(int){ 2 }, sizeof(int));
	setsockopt(conn_base->fd, IPPROTO_TCP, TCP_KEEPIDLE, &(int){ 2 }, sizeof(int));
	setsockopt(conn_base->fd, IPPROTO_TCP, TCP_KEEPINTVL, &(int){ 1 }, sizeof(int));
		
	LOG_DEBUG(CONN_LOG_LEVEL, "Connected. %p %d %s %s", conn_base, conn_base->fd, uaddr_readable(&conn_base->addr), uaddr_readable(addr));
	return conn_base;
}

void* conn_recv(void *conn, uint32_t max_buffers, time_t cur_time, void **new_buf){
	uint32_t buffer_size = CONN_BUFFER_SIZE(conn);
	CONN_T(buffer_size) *conn_base = conn;
	if (conn_base->recv_buffers.size >= max_buffers - 1 || 
		conn_base->recv_buffers.size + conn_base->send_buffers.size >= max_buffers){
		return conn_base;
	}
	CONN_BUF_T(buffer_size) *buf = llist_take(&conn_base->recv_buffers);
	if (buf == NULL){
		return conn_base;
	}	
	buf->meta.bytes_received = recv(conn_base->fd, buf->data, buffer_size, 0);
	if (buf->meta.bytes_received < 0 && errno != EWOULDBLOCK && errno != EAGAIN){
		LOG_INFO(CONN_LOG_LEVEL, "Connection hanged during recv. %p %d %s [%d,%s]", conn_base, conn_base->fd, uaddr_readable(&conn_base->addr), errno, strerror(errno));
		return NULL;
	} else if (buf->meta.bytes_received == 0) {
		LOG_INFO(CONN_LOG_LEVEL, "No data from connection. Connection closed. %p %d %s", conn_base, conn_base->fd, uaddr_readable(&conn_base->addr));
		return NULL;
	} else if (buf->meta.bytes_received > 0){
		LOG_DEBUG(CONN_LOG_LEVEL, "Received %ld bytes from connection. %p %d %s", buf->meta.bytes_received, conn_base, conn_base->fd, uaddr_readable(&conn_base->addr));
		conn_base->last_active = cur_time;
		buf->meta.time_received = cur_time;
		buf->meta.bytes_sended = 0;
		*new_buf = buf;
		return conn_base;
	} 
	LOG_DEBUG(CONN_LOG_LEVEL, "EWOULDBLOCK or EAGAIN on connection. %p %d %s", conn_base, conn_base->fd, uaddr_readable(&conn_base->addr));
	llist_release(&conn_base->recv_buffers, buf);//(EWOULDBLOCK or EAGAIN)	
	return conn_base;
}

void* conn_send(void *conn, time_t cur_time){
	uint32_t buffer_size = CONN_BUFFER_SIZE(conn);
	CONN_T(buffer_size) *conn_base = conn;
	CONN_BUF_T(buffer_size) *buf = llist_get_first(&conn_base->send_buffers);
	if (buf == NULL){
		return conn_base;
	}
	ssize_t bytes_sended = send(conn_base->fd, buf->data + buf->meta.bytes_sended, buf->meta.bytes_received - buf->meta.bytes_sended, 0);
	if (bytes_sended < 0 && errno != EWOULDBLOCK && errno != EAGAIN){
		LOG_INFO(CONN_LOG_LEVEL, "Connection hanged during send. %p %d %s [%d,%s]", conn, conn_base->fd, uaddr_readable(&conn_base->addr), errno, strerror(errno));
		return NULL;
	} else if (bytes_sended == 0){
		LOG_INFO(CONN_LOG_LEVEL, "Can not send data to connection. Connection closed. %p %d %s", conn, conn_base->fd, uaddr_readable(&conn_base->addr));
		return NULL;
	} else if (bytes_sended > 0) {
		LOG_DEBUG(CONN_LOG_LEVEL, "%ld bytes of %ld sended to connection. %p %d %s", bytes_sended, buf->meta.bytes_received, conn, conn_base->fd, uaddr_readable(&conn_base->addr));
		buf->meta.bytes_sended += bytes_sended;
		if (buf->meta.bytes_sended == buf->meta.bytes_received){
			llist_release(&conn_base->send_buffers, buf);
		} else {
			buf->meta.time_received = cur_time;
		}
		conn_base->last_active = cur_time;
		return conn_base;
	}
	LOG_DEBUG(CONN_LOG_LEVEL, "EWOULDBLOCK or EAGAIN on connection. %p %d %s", conn_base, conn_base->fd, uaddr_readable(&conn_base->addr));
	return conn_base;
}

void* conn_copy_buffer(void* conn_dst, void* src, uint32_t max_buffers){
	uint32_t buffer_size = CONN_BUFFER_SIZE(conn_dst);
	CONN_T(buffer_size) *conn_base = conn_dst;
	if (conn_base->send_buffers.size >= max_buffers - 1 ||
		conn_base->recv_buffers.size + conn_base->send_buffers.size >= max_buffers){
		return NULL;
	}
	CONN_BUF_T(buffer_size) *buf_to = llist_take(&conn_base->send_buffers);
	if (buf_to == NULL){
		return NULL;
	}
	CONN_BUF_T(buffer_size) *buf_from = src;
	buf_to->meta.time_received = buf_from->meta.time_received;
	buf_to->meta.bytes_sended = buf_from->meta.bytes_sended;
	buf_to->meta.bytes_received = buf_from->meta.bytes_received;
	memcpy(buf_to->data, buf_from->data, buf_from->meta.bytes_received);
	return conn_base;
}
