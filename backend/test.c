#include "backend.h"
#include "../connection/connection.h"

#include "backend.c"
#include "../config_reader/config_reader.c"
#include "../mp_config/mp_config.c"
#include "../connection/connection.c"
#include "../linked_list/linked_list.c"
#include "../uni_addr/uni_addr.c"

int main(){
	int buffer_size = 1024;
	backend_params_t params;
	params.max_approved_threads = 1;
	params.max_approved_addr_threads = 0;
	params.max_approved_addrs = 0;
	params.max_addrs = 10;
	params.max_threads = 2;
	params.max_addr_threads = 5;
	strncpy(params.name, "name123", MAX_GROUP_NAME);
	strncpy(params.host, "google.com", MAX_GROUP_NAME);
	
	backend_t *backend = alloc_backend(&params, SOCK_DGRAM, 10, 10, buffer_size);
	
	printf("Test1\n");
	CONN_T(buffer_size) *conn_client = alloc_conn(CONN_TYPE_CLIENT, buffer_size, time(0));
	conn_client->approved = false;
	uaddr_fill("google.com", &conn_client->addr, SOCK_STREAM);

	be_add_conn(backend, conn_client);
	printf("%p\n", be_approve_conn(backend, conn_client, conn_client));
	printf("%p\n", be_approve_conn(backend, conn_client, conn_client));
	be_remove_conn(backend, conn_client);
	free_conn(conn_client);
	
	printf("Test2\n");
	CONN_BUF_T(buffer_size) buffer;
	char buffer_data[3] = "123";
	buffer.meta.time_received = time(0);
	buffer.meta.bytes_received = sizeof(buffer_data);
    memcpy(buffer.data, buffer_data, sizeof(buffer_data));
	printf("%p\n", be_get_cache(backend, (void*)&buffer, 10));
	be_add_cache(backend, (void*)&buffer);
	be_add_cache(backend, (void*)&buffer);
	printf("%p\n", be_get_cache(backend, (void*)&buffer, 10));
	
	free_backend(backend);
}