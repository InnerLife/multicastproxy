/** @file backend.h
 *  @brief Backend(not connection) as a file data representation. Additionally contains cache for fast client initialization and pointers to client connections uses backend to check limits.
 */
#ifndef H_BACKEND
#define H_BACKEND

#include "../log/log.h"
#include "../mp_config/mp_config.h"
#include "../linked_list/linked_list.h"
#include "../uni_addr/uni_addr.h"
#include <pthread.h>
#include <stdbool.h>
/**
 * @brief Collection of client connections pointers grouped by their ip(without port)
 */
typedef struct addr_conns {
	uni_addr_t addr;
	llist_t conns;
} addr_conns_t;

typedef struct backend {
	pthread_mutex_t lock; /**< keep thread-safety on variable addr_conns list(critical), connection approving and removing(critical); better to use separate locks if it will be a bottleneck */
	pthread_mutex_t cache_lock; /**< keep operations thread-safe on variable cache list; the first predictable bottleneck in case of one-for-all lock */
	backend_params_t params; /**< backend params */
	uni_addr_t addr; /**< backend addr */
	llist_t *addr_conns; /**< collection of ip_conns_t. Client connection tracking. */
	llist_t *cache; /**< collection of CONN_BUF_T */
	bool closed; /**< Was backend removed from file. */
} backend_t;

/**
 * @brief Initialize already allocated backend instance
 * 
 * @param backend allocated backend instance
 * @param params - backend params
 * @param type - backend addr type (SOCK_DGRAM, SOCK_STREAM, ...)
 * @param conn_prealloc - target client connections count in a whole system 
 * @param cache_prealloc - cache size, fixed
 * @param buffer_size - connection buffer size (for cache preallocation)
 * @return backend if success, otherwise - NULL
 */
backend_t* init_backend(backend_t *backend, backend_params_t *params, int type, uint32_t conn_prealloc, uint32_t cache_prealloc, uint32_t buffer_size);
/**
 * @brief Allocate and initialize backend instance
 * 
 * @param backend allocated backend instance
 * @param params - backend params
 * @param type - backend addr type (SOCK_DGRAM, SOCK_STREAM, ...)
 * @param conn_prealloc - target client connections count in a whole system 
 * @param cache_prealloc - cache size, fixed
 * @param buffer_size - connection buffer size (for cache preallocation)
 * @return backend if success, otherwise - NULL
 */
backend_t* alloc_backend(backend_params_t *params, int type, uint32_t conn_prealloc, uint32_t cache_prealloc, uint32_t buffer_size);
/**
 * @brief Free backend internal data structures
 * 
 * @param backend backend instance
 */
void free_backend_data(backend_t *backend);
/**
 * @brief Free backend internal data structures and backend ptr itself
 * 
 * @param backend backend instance
 */
void free_backend(backend_t *backend);

/**
 * @brief Logically close backend
 * 
 * @param backend backend instance
 */
void be_close(backend_t *backend);
/**
 * @brief Compare two backend params instances for equality 
 * 
 * @param params1 first params
 * @param params2 second params
 * @return true if params are equal
 */
bool be_cmp_params(backend_params_t *params1, backend_params_t *params2);
/**
 * @brief Check if connection with same addr already exists in backend 
 * 
 * @param backend backend
 * @param conn client connection
 * @return true if exist
 */
bool be_conn_exist(backend_t *backend, void *conn);
/**
 * @brief Add client connection to tracking list
 * 
 * @param backend backend
 * @param conn client connection
 * @return backend if success, NULL - allocation error, limits exceeded
 */
backend_t* be_add_conn(backend_t *backend, void *conn);
/**
 * @brief Remove client connection from tracking list
 * 
 * @param backend backend
 * @param conn client connection
 */
void be_remove_conn(backend_t *backend, void *conn);
/**
 * @brief Approve client connection checking backend connection limits
 * 
 * @param backend backend
 * @param conn client connection
 * @return backend if success, NULL - limits exceeded
 */
backend_t* be_approve_conn(backend_t *backend, void *conn, void *approved);

/**
 * @brief Add buffer to backend cache
 * 
 * @param backend backend
 * @param buffer client connection buffer
 */
void be_add_cache(backend_t *backend, void *buffer);
/**
 * @brief Check buffer cache
 * 
 * @param backend backend
 * @param buffer client connection buffer
 * @param cache_ttl cache ttl
 * @return backend if buffer found in cache, otherwise - NULL
 */
backend_t* be_get_cache(backend_t *backend, void *buffer, uint32_t cache_ttl);

#endif

#ifndef BE_LOG_LEVEL
#define BE_LOG_LEVEL C_LOG_DEFAULT
#endif
