#include "backend.h"
#include "../connection/connection.h"

#include <stdlib.h>

backend_t* init_backend(backend_t *backend, backend_params_t *params, int type, uint32_t conn_prealloc, uint32_t cache_prealloc, uint32_t buffer_size){
	backend->closed = false;
	memcpy(&backend->params, params, sizeof(*params));
	//memset(&backend->addr, 0, sizeof(backend->addr));
	if (uaddr_fill(params->host, &backend->addr, type) == NULL){
		LOG_DEBUG(BE_LOG_LEVEL, "Error getting backend address. %p", backend);
		return NULL;
	}	
	backend->addr.sin.sin_port = htons(params->port);
	if (pthread_mutex_init(&backend->lock, NULL) != 0){
		LOG_DEBUG(BE_LOG_LEVEL, "Error initializing lock for backend. %p", backend);
		return NULL;
	}
	if (pthread_mutex_init(&backend->cache_lock, NULL) != 0){
		LOG_DEBUG(BE_LOG_LEVEL, "Error initializing cache lock for backend. %p", backend);
		pthread_mutex_destroy(&backend->lock);
		return NULL;
	}	
	uint32_t ip_prealloc_size = 0;
	if (params->max_addrs > 0){
		ip_prealloc_size = params->max_addrs;
	} else if (params->max_threads > 0){
		if (params->max_addr_threads > 0){
			ip_prealloc_size = params->max_threads / params->max_addr_threads;
		} else {
			ip_prealloc_size = (params->max_addr_threads < conn_prealloc) ? (params->max_addr_threads) : (conn_prealloc);
		}
	} else {
		if (params->max_addr_threads > 0){
			ip_prealloc_size = conn_prealloc / params->max_addr_threads;
		} else {
			ip_prealloc_size = conn_prealloc;
		}
	}
	LOG_DEBUG(BE_LOG_LEVEL, "Preallocating connnections for backend. %p %d", backend, ip_prealloc_size);
	backend->addr_conns = alloc_llist(sizeof(addr_conns_t), ip_prealloc_size);
	if (backend->addr_conns == NULL){
		LOG_ERROR(BE_LOG_LEVEL, "Can't allocate backend connections list.");
		pthread_mutex_destroy(&backend->lock);
		pthread_mutex_destroy(&backend->cache_lock);
		return NULL;
	}	
	if (cache_prealloc == 0){
		backend->cache = NULL;
		return backend;
	}
	backend->cache = alloc_llist(sizeof(CONN_BUF_T(buffer_size)), cache_prealloc);
	if (backend->cache == NULL){
		LOG_ERROR(BE_LOG_LEVEL, "Can't allocate backend cache.");
		pthread_mutex_destroy(&backend->lock);
		pthread_mutex_destroy(&backend->cache_lock);
		free_llist(backend->addr_conns);
		return NULL;
	}
	LOG_DEBUG(BE_LOG_LEVEL, "Backend initialized. %p %s", backend, backend->params.name);
	return backend;
}

backend_t* alloc_backend(backend_params_t *params, int type, uint32_t conn_prealloc, uint32_t cache_prealloc, uint32_t buffer_size){
	LOG_DEBUG(BE_LOG_LEVEL, "Allocating backend...");
	backend_t *backend = malloc(sizeof(*backend));
	if (backend == NULL){
		LOG_ERROR(BE_LOG_LEVEL, "Error allocating memory for backend.");
		return NULL;
	}
	LOG_DEBUG(BE_LOG_LEVEL, "Backend allocated. %p", backend);
	if (init_backend(backend, params, type, conn_prealloc, cache_prealloc, buffer_size) == NULL){
		free(backend);
		return NULL;
	}
	return backend;
}

void free_backend_data(backend_t *backend){
	llist_iterator_t iter;
	for (addr_conns_t *addr_conns = llist_iter_init(&iter, backend->addr_conns); addr_conns != NULL; addr_conns = llist_iter_next(&iter)){
		free_llist_data(&addr_conns->conns);
	}
	free_llist(backend->addr_conns);
	if (backend->cache != NULL){
		free_llist(backend->cache);
	}
	pthread_mutex_destroy(&backend->lock);
	pthread_mutex_destroy(&backend->cache_lock);
	LOG_DEBUG(BE_LOG_LEVEL, "Backend data freed. %p %s", backend, backend->params.name);
}

void free_backend(backend_t *backend){
	free_backend_data(backend);
	free(backend);
	LOG_DEBUG(BE_LOG_LEVEL, "Backend freed. %p", backend);
}

void be_close(backend_t *backend){
	//backend can be closed from only one thread, closed state can not be reverted, so it's safe to check for closed state without lock
	if (backend->closed){
		LOG_DEBUG(BE_LOG_LEVEL, "No need to close backend. %s", backend->params.name);
		return;
	}	
	backend->closed = true;
	LOG_DEBUG(BE_LOG_LEVEL, "Backend closed. %p %s", backend, backend->params.name);
}

bool be_cmp_params(backend_params_t *params1, backend_params_t *params2){
	return (params1->port == params2->port && strcmp(params1->host, params2->host) == 0);
}

static void get_counts(backend_t *backend, void *conn, bool approved, uint32_t *same_addr, uint32_t *addrs, uint32_t *all){
	CONN_TT(conn) *conn_base = conn;
	llist_iterator_t iter_c;
	llist_iterator_t iter_ip;
	for (addr_conns_t *addr_conns = llist_iter_init(&iter_ip, backend->addr_conns); addr_conns != NULL; addr_conns = llist_iter_next(&iter_ip)){
		bool same = uaddr_cmp(&conn_base->addr, &addr_conns->addr, false);
		uint32_t all_tmp = (*all);
		for (void **conn_p = llist_iter_init(&iter_c, &addr_conns->conns); conn_p != NULL; conn_p = llist_iter_next(&iter_c)){
			CONN_TT(conn_p) *conn_i = *conn_p;
			if ((approved && conn_i->approved != NULL) || !approved){
				(*all)++;
				if (same){
					(*same_addr)++;
				}
			}
		}
		//inc addrs count only if "if" statement above was true
		if (*all > all_tmp){
			(*addrs)++;
		}
	}
}

static backend_t* check_limits(backend_t *backend, void *conn, bool approving_phase, uint32_t same_addr_limit, uint32_t addrs_limit, uint32_t all_limit){
	if (backend->closed){
		return NULL;
	}
	CONN_TT(conn) *conn_base = conn;
	if (same_addr_limit <= 0 && addrs_limit <= 0 && all_limit <= 0){
		LOG_DEBUG(BE_LOG_LEVEL, "No need to check connection limits. Approving phase(?): %s. %p %d %s %s", (approving_phase) ? "true" : "false",
			conn_base, conn_base->fd, uaddr_readable(&conn_base->addr), backend->params.name);
		return backend;
	}
	
	uint32_t same_addr = 0;
	uint32_t addrs = 0;
	uint32_t all = 0;
	get_counts(backend, conn, approving_phase, &same_addr, &addrs, &all);
	
	if (addrs_limit > 0 && addrs_limit <= addrs){
		LOG_INFO(BE_LOG_LEVEL, "Max distinct ips count exceeded. Approving phase(?): %s. %p %d %s %s", (approving_phase) ? "true" : "false", 
			conn_base, conn_base->fd, uaddr_readable(&conn_base->addr), backend->params.name);
		return NULL;
	}
	if (all_limit > 0 && all_limit <= all){
		LOG_INFO(BE_LOG_LEVEL, "Max threads count exceeded. Approving phase(?): %s. %p %d %s %s", (approving_phase) ? "true" : "false", 
			conn_base, conn_base->fd, uaddr_readable(&conn_base->addr), backend->params.name);
		return NULL;
	}
	if (same_addr_limit > 0 && same_addr_limit <= same_addr){
		LOG_INFO(BE_LOG_LEVEL, "Max threads per ip count exceeded. Approving phase(?): %s. %p %d %s %s", (approving_phase) ? "true" : "false", 
			conn_base, conn_base->fd, uaddr_readable(&conn_base->addr), backend->params.name);
		return NULL;
	}
	LOG_DEBUG(BE_LOG_LEVEL, "Connection passed limits check. Approving phase(?): %s. %p %d %s %s %d %d %d", (approving_phase) ? "true" : "false", 
		conn_base, conn_base->fd, uaddr_readable(&conn_base->addr), backend->params.name, addrs, all, same_addr);
	return backend;
}

static addr_conns_t* conn_exist(backend_t *backend, void *conn){
	CONN_TT(conn) *conn_base = conn;
	llist_iterator_t iter;
	for (addr_conns_t *addr_conns = llist_iter_init(&iter, backend->addr_conns); addr_conns != NULL; addr_conns = llist_iter_next(&iter)){
		if (uaddr_cmp(&conn_base->addr, &addr_conns->addr, false)){
			return addr_conns;
		}
	}
	return NULL;
}

bool be_conn_exist(backend_t *backend, void *conn){
	pthread_mutex_lock(&backend->lock);
	addr_conns_t *res = conn_exist(backend, conn);
	pthread_mutex_unlock(&backend->lock);
	return res != NULL;
}

static backend_t* add_conn(backend_t *backend, void *conn){
	CONN_TT(conn) *conn_base = conn;
	if (check_limits(backend, conn_base, false, backend->params.max_addr_threads, backend->params.max_addrs, backend->params.max_threads) == NULL){
		return NULL;
	}
	addr_conns_t *exist = conn_exist(backend, conn);
	if (exist == NULL){
		exist = llist_take(backend->addr_conns);
		if (exist == NULL){
			LOG_ERROR(BE_LOG_LEVEL, "Can't allocate connections per ip list.");
			return NULL;
		}
		memcpy(&exist->addr, &conn_base->addr, sizeof(conn_base->addr));
		init_llist(&exist->conns, sizeof(void**), 0);
		LOG_DEBUG(BE_LOG_LEVEL, "New backend connections list created. %p %d %s %s", conn_base, conn_base->fd, uaddr_readable(&conn_base->addr), backend->params.name);
	}
	void **conn_p = llist_take(&exist->conns);
	if (conn_p == NULL){
		LOG_ERROR(BE_LOG_LEVEL, "Can't allocate connection pointer.");
		if (exist->conns.size == 0){
			free_llist_data(&exist->conns);
			llist_release(backend->addr_conns, exist);
		}
		return NULL;
	}
	*conn_p = conn_base;
	LOG_DEBUG(BE_LOG_LEVEL, "Connection added to backend. %p %d %s %s", conn_base, conn_base->fd, uaddr_readable(&conn_base->addr), backend->params.name);
	return backend;
}

backend_t* be_add_conn(backend_t *backend, void *conn){
	pthread_mutex_lock(&backend->lock);
	backend_t *res = add_conn(backend, conn);
	pthread_mutex_unlock(&backend->lock);
	return res;
}

static void remove_conn(backend_t *backend, void *conn){
	CONN_TT(conn) *conn_base = conn;
	//no check for closed backend, allow remove
	addr_conns_t *exist = conn_exist(backend, conn);
	if (exist == NULL){
		LOG_DEBUG(BE_LOG_LEVEL, "Connection ip not found in backend. %p %d %s %s", conn_base, conn_base->fd, uaddr_readable(&conn_base->addr), backend->params.name);
		return;
	}
	llist_iterator_t iter;
	for (void **conn_p = llist_iter_init(&iter, &exist->conns); conn_p != NULL; conn_p = llist_iter_next(&iter)){
		if (conn_base == *conn_p){
			llist_release(&exist->conns, conn_p);
			if (exist->conns.size == 0){
				free_llist_data(&exist->conns);
				llist_release(backend->addr_conns, exist);
			}	
			LOG_DEBUG(BE_LOG_LEVEL, "Connection removed from backend. %p %d %s %s", conn_base, conn_base->fd, uaddr_readable(&conn_base->addr), backend->params.name);
			return;
		}
	}
	LOG_DEBUG(BE_LOG_LEVEL, "Connection not found in backend. %p %d %s %s", conn_base, conn_base->fd, uaddr_readable(&conn_base->addr), backend->params.name);
}

void be_remove_conn(backend_t *backend, void *conn){
	pthread_mutex_lock(&backend->lock);
	remove_conn(backend, conn);
	pthread_mutex_unlock(&backend->lock);
}

backend_t* be_approve_conn(backend_t *backend, void *conn, void *approved){
	CONN_TT(conn) *conn_base = conn;
	pthread_mutex_lock(&backend->lock);
	backend_t *res = check_limits(backend, conn_base, true, backend->params.max_approved_addr_threads, backend->params.max_approved_addrs, backend->params.max_approved_threads);
	if (res != NULL){
		conn_base->approved = approved;
	}
	pthread_mutex_unlock(&backend->lock);
	return res;
}

static void add_cache(backend_t *backend, void *buffer){
	CONN_BUF_T(1) *conn_buffer = buffer;
	llist_iterator_t iter;
	for (CONN_BUF_T(1) *cache_buffer = llist_iter_init(&iter, backend->cache); cache_buffer != NULL; cache_buffer = llist_iter_next(&iter)){
		if (cache_buffer->meta.bytes_received == conn_buffer->meta.bytes_received && memcmp(cache_buffer->data, conn_buffer->data, conn_buffer->meta.bytes_received) == 0){
			LOG_DEBUG(BE_LOG_LEVEL, "Found existing cache entry. %s %ld", backend->params.name, conn_buffer->meta.bytes_received);
			llist_to_last(backend->cache, cache_buffer);
			cache_buffer->meta.time_received = conn_buffer->meta.time_received;
			return;
		}
	}
	if (backend->cache->free == NULL){
		llist_release(backend->cache, llist_get_first(backend->cache));
	}
	CONN_BUF_T(1) *cache_buffer = llist_take(backend->cache);
	cache_buffer->meta.time_received = conn_buffer->meta.time_received;
	cache_buffer->meta.bytes_received = conn_buffer->meta.bytes_received;
	memcpy(cache_buffer->data, conn_buffer->data, conn_buffer->meta.bytes_received);
	LOG_DEBUG(BE_LOG_LEVEL, "New cache entry created. %s %ld", backend->params.name, conn_buffer->meta.bytes_received);
}

void be_add_cache(backend_t *backend, void *buffer){
	if (backend->cache == NULL){
		LOG_DEBUG(BE_LOG_LEVEL, "No need to add cache. %s", backend->params.name);
		return;
	}
	//no close lock aquiring to speed up
	pthread_mutex_lock(&backend->cache_lock);
	add_cache(backend, buffer);
	pthread_mutex_unlock(&backend->cache_lock);
}

static backend_t* get_cache(backend_t *backend, void *buffer, uint32_t cache_ttl){
	CONN_BUF_T(1) *conn_buffer = buffer;
	llist_iterator_t iter;
	time_t cur_time = time(0);
	for (CONN_BUF_T(1) *cache_buffer = llist_iter_init(&iter, backend->cache); cache_buffer != NULL; cache_buffer = llist_iter_next(&iter)){
		if (cache_buffer->meta.bytes_received == conn_buffer->meta.bytes_received && 
			cache_buffer->meta.bytes_received + cache_ttl > cur_time && 
			memcmp(cache_buffer->data, conn_buffer->data, conn_buffer->meta.bytes_received) == 0
			){
			LOG_DEBUG(BE_LOG_LEVEL, "Cache entry found. %s %ld", backend->params.name, conn_buffer->meta.bytes_received);
			return backend;
		}
	}
	LOG_DEBUG(BE_LOG_LEVEL, "Cache entry not found. %s %ld", backend->params.name, conn_buffer->meta.bytes_received);
	return NULL;
}

backend_t* be_get_cache(backend_t *backend, void *buffer, uint32_t cache_ttl){
	if (backend->cache == NULL){
		LOG_DEBUG(BE_LOG_LEVEL, "No need to check cache. %s", backend->params.name);
		return NULL;
	}
	pthread_mutex_lock(&backend->cache_lock);
	backend_t *res = get_cache(backend, buffer, cache_ttl);
	pthread_mutex_unlock(&backend->cache_lock);
	return res;
}
