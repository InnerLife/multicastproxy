# Multicast Proxy Server
Multicast Proxy Server (MPServer) is a hybrid of Reverse Proxy and Deep Packet Inspection (DPI).
Unlike classic Reverse Proxy, it can redirect client requests to multiple backend servers behind ONE port.
The right backend server is selected on-the-fly using DPI of protocol negotiation phase. After selection MPServer acts like a classic Reverse Proxy.
In case you have only one backend server, MPServer can act (if you want it to) like a classic TCP or UDP Reverse Proxy without any DPI.
MPServer is not a classic Load Balancer, but can act as a dumb one. Client requests will be redirected to first replied backend server.
MPServer can possibly act like a [SSLH](https://github.com/yrutschle/sslh) multiplexer.

### How it works
MPServer duplicates client requests to all backend servers. Then, it starts to filter backend servers replies, choosing the first valid reply and sending it to client. Backend servers replies are filtering out using regex-based DPI.
Usually, you only need to define such regex according to your protocol specs.
OpenVPN(TCP and UDP), SOCKS5, HTTP and L2TP/IPSec sample configs (with regex) are available in examples directory.

### SOCKS5 example
Goal: Authenticate client on one of the backend servers. Filter out the rest of servers.

Client sends to MPServer SOCKS5 Auth request which looks like this (HEX):
```sh
050102 # 05 - Protocol version; 01 - Auth method count; 02 - Auth by user/pass
```
MPServer duplicates it and sends to all of backend servers.
Then it start to receive replies from servers. Imagine all of servers reply this:
```sh
0502 # 05 - Protocol version; 02 - Accepted auth method (user/pass)
```
Which means all of servers supports this auth method and waiting for auth.

![stage1](http://innerlife.io/wp-content/uploads/2020/08/mpserver_stage1.png)

Client sends auth data:
```sh
0104757365720470617373 # 01 - Subnegotiation Version; 0475736572 - user; 0470617373 - pass
```
MPServer again send it to all servers and starting to receive their replies.
Imagine, first server replied this:
```sh
0100 # 01 - Subnegotiation Version; 00 - Success
```
Which means client was successfully authenticated.
We don't need to wait for rest of replies. We can filter other servers out by defining the following rule in MPServer config:
```sh
[APPROVE_RULES]
auth_success ^0100
```

![stage1](http://innerlife.io/wp-content/uploads/2020/08/mpserver_stage2.png)

From this time client communicates (through MPServer) with only one server and MPServer acts like a classic Reverse Proxy.

### Installation
gcc should be installed
```sh
git clone https://bitbucket.org/InnerLife/multicastproxy
cd multicastproxy
make
```

### Running
```sh
mp_server config.conf
```
MPServer has no daemon mode, but you can run it as daemon using systemd.
Example of systemd.service script is included.

### IPSec
Due to some protocols design, you can get different backend server replies for the same client requests. One of them is IPSec.
Here is a patched version of [Strongswan](https://bitbucket.org/InnerLife/strongswan/src/shared-sa/) ipsec-server which able to work with MPServer.