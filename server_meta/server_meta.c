#include "server_meta.h"
#include "../connection/connection_backend.h"

#include <stdlib.h>

#define HEX_SIZE(size) size * 2 + 1

static creader_t* add_rules(creader_t *reader, rule_t *rules, int64_t (*get_func)(creader_t*,rule_params_t**)){
	memset(rules, 0, sizeof(*rules));
	rule_params_t *rule_params;
	int rules_count = get_func(reader, &rule_params);
	if (rules_count < 0) {
		return NULL;
	}
	for (int i = 0; i < rules_count; i++){
		rule_t *rule = rules + i;
		rule_params_t *params = rule_params + i;
		if (regcomp(&rule->regex, params->regex, REG_EXTENDED) != 0){
			LOG_ERROR(SM_LOG_LEVEL, "Can not compile rule. %s %s", params->name, params->regex);
			for (int j = 0; j < i; j++){
				regfree(&rule->regex);
			}				
			return NULL;
		}
		memcpy(&rule->params, params, sizeof(*params));
		LOG_INFO(SM_LOG_LEVEL, "Rule added. %s %s", params->name, params->regex);
	}	
	return reader;
}

static void free_rules(rule_t *rules){
	for (int i = 0; i < MAX_RULES; i++){
		rule_t *rule = rules + i;
		if (strlen(rule->params.name) == 0){
			break;
		}
		regfree(&rule->regex);
	}
}
/*----------------------------------------------------------------------*/
server_meta_t* init_smeta(server_meta_t *smeta, creader_t *reader){
	params_t *params = mpc_get_params(reader);
	if (params == NULL) {
		LOG_ERROR(SM_LOG_LEVEL, "Can't read params from server meta.");
		return NULL;
	}
	smeta->cache_ttl = params->cache_ttl;
	smeta->cache_size = params->cache_size;
	smeta->addr_cache = params->addr_cache;
	smeta->clients_prealloc = params->clients_prealloc;
	smeta->type = params->type;
	smeta->buffer_size = params->buffer_size;
	strncpy(smeta->backends_file, params->backends_file, MAX_BACKENDS_FILE_NAME);
	memset(&smeta->approve_rules, 0, sizeof(smeta->approve_rules));
	memset(&smeta->close_rules, 0, sizeof(smeta->close_rules));
	memset(&smeta->close_client_rules, 0, sizeof(smeta->close_client_rules));
	if (pthread_mutex_init(&smeta->lock, NULL) != 0){
		LOG_DEBUG(SM_LOG_LEVEL, "Error initializing lock for server meta.");
		return NULL;
	}
	smeta->backends = alloc_llist(sizeof(backend_t), BACKENDS_PREALLOC);
	if (smeta->backends == NULL){
		LOG_ERROR(SM_LOG_LEVEL, "Error allocating memory for backends.");
		pthread_mutex_destroy(&smeta->lock);
		return NULL;
	}
	if (smeta_load_backends(smeta) == NULL){
		LOG_ERROR(SM_LOG_LEVEL, "Can't load backends.");
		pthread_mutex_destroy(&smeta->lock);
		free_llist(smeta->backends);
		return NULL;
	}
	if (add_rules(reader, smeta->approve_rules, &mpc_get_approve_rules) == NULL){
		LOG_ERROR(SM_LOG_LEVEL, "Can't read approve rules.");
		pthread_mutex_destroy(&smeta->lock);
		free_llist(smeta->backends);
		return NULL;
	}
	if (add_rules(reader, smeta->close_rules, &mpc_get_close_rules) == NULL){
		LOG_ERROR(SM_LOG_LEVEL, "Can't read close rules.");
		pthread_mutex_destroy(&smeta->lock);
		free_llist(smeta->backends);
		free_rules(smeta->approve_rules);
		return NULL;
	}
	if (add_rules(reader, smeta->close_client_rules, &mpc_get_close_client_rules) == NULL){
		LOG_ERROR(SM_LOG_LEVEL, "Can't read close client rules.");
		pthread_mutex_destroy(&smeta->lock);
		free_llist(smeta->backends);
		free_rules(smeta->approve_rules);
		free_rules(smeta->close_rules);
		return NULL;
	}
	LOG_DEBUG(SM_LOG_LEVEL, "Server meta initialized.");
	return smeta;
}

server_meta_t* alloc_smeta(creader_t *reader){
	LOG_DEBUG(LL_LOG_LEVEL, "Allocating server meta...");
	server_meta_t *smeta = malloc(sizeof(*smeta));
	if (smeta == NULL){
		LOG_ERROR(SM_LOG_LEVEL, "Error allocating memory for server meta.");
		return NULL;
	}
	LOG_DEBUG(SM_LOG_LEVEL, "Server meta allocated.");
	if (init_smeta(smeta, reader) == NULL){
		free(smeta);
		return NULL;
	}
	return smeta;
}

void free_smeta_data(server_meta_t *smeta){
	llist_iterator_t iter;
	for (backend_t *backend = llist_iter_init(&iter, smeta->backends); backend != NULL; backend = llist_iter_next(&iter)){
		be_close(backend);
		//actually, should wait here before all backend connections will close or close all connections manually before calling
		free_backend_data(backend);
		llist_release(smeta->backends, backend);
	}
	free(smeta->backends);
	free_rules(smeta->approve_rules);
	free_rules(smeta->close_rules);
	free_rules(smeta->close_client_rules);
	LOG_DEBUG(SM_LOG_LEVEL, "Server meta data freed.");
}

void free_smeta(server_meta_t *smeta){
	free_smeta_data(smeta);
	free(smeta);
	LOG_DEBUG(SM_LOG_LEVEL, "Server meta freed.");
}
/*----------------------------------------------------------------------*/

static backend_t* add_backend(server_meta_t *smeta, backend_params_t *params, uint32_t backends_size){
	backend_t *backend = llist_take(smeta->backends);
	if (backend == NULL){
		LOG_ERROR(SM_LOG_LEVEL, "Can't allocate new backend.");
		return NULL;
	}
	if (init_backend(backend, params, smeta->type, smeta->clients_prealloc / backends_size, smeta->cache_size, smeta->buffer_size) == NULL){
		LOG_ERROR(SM_LOG_LEVEL, "Can't init new backend.");
		llist_release(smeta->backends, backend);
		return NULL;
	}
	
	LOG_INFO(SM_LOG_LEVEL, "New backend added. %p %s %s", backend, backend->params.name, uaddr_readable(&backend->addr));
	return backend;
}

static server_meta_t* merge_backends(server_meta_t *smeta, uint32_t backends_size, backend_params_t *backend_params){
	llist_iterator_t iter;
	for (backend_t *backend = llist_iter_init(&iter, smeta->backends); backend != NULL; backend = llist_iter_next(&iter)){ 
		backend_params_t *exists = NULL;
		for(int i = 0; i < backends_size; i++){
			backend_params_t *params = backend_params + i;
			if (be_cmp_params(&backend->params, params)){
				exists = params;
				break;
			}
		}
		if (exists != NULL){
			backend->params.max_approved_threads = exists->max_approved_threads;
			backend->params.max_approved_addr_threads = exists->max_approved_addr_threads;
			backend->params.max_approved_addrs = exists->max_approved_addrs;
			backend->params.max_threads = exists->max_threads;
			backend->params.max_addr_threads = exists->max_addr_threads;
			backend->params.max_addrs = exists->max_addrs;
			strncpy(backend->params.name, exists->name, MAX_GROUP_NAME);
		} else {
			LOG_INFO(SM_LOG_LEVEL, "Removing old backend. %p %s", backend, backend->params.name);
			be_close(backend);			
		}
		if (backend->closed && backend->addr_conns->size == 0){
			pthread_mutex_lock(&smeta->lock);
			free_backend_data(backend);
			llist_release(smeta->backends, backend);
			pthread_mutex_unlock(&smeta->lock);
		}
	}
	for(int i = 0; i < backends_size; i++){
		backend_params_t *params = backend_params + i;
		bool exists = false;
		llist_iterator_t iter;
		for (backend_t *backend = llist_iter_init(&iter, smeta->backends); backend != NULL; backend = llist_iter_next(&iter)){ 
			if (be_cmp_params(&backend->params, params)){
				exists = true;
				break;
			}
		}
		if (!exists){
			pthread_mutex_lock(&smeta->lock);
			if (add_backend(smeta, params, backends_size) == NULL){
				pthread_mutex_unlock(&smeta->lock);
				return NULL;
			}
			pthread_mutex_unlock(&smeta->lock);
		}
	}
	return smeta;
}

server_meta_t* smeta_load_backends(server_meta_t *smeta){
	creader_t *reader = alloc_creader();
	if (reader == NULL){
		LOG_ERROR(SM_LOG_LEVEL, "Can not alloc backends reader.");
		return NULL;
	}
	if (cr_read_file(reader, smeta->backends_file) == NULL){
		LOG_ERROR(SM_LOG_LEVEL, "Can not read backends file.");
		free_creader(reader);
		return NULL;
	}	
	backend_params_t *backend_params;
	uint32_t backends_size = mpc_get_backends(reader, &backend_params);
	if (backends_size < 0) {
		LOG_ERROR(SM_LOG_LEVEL, "Can't read backends params.");
		free_creader(reader);
		return NULL;
	}
	free_creader(reader);
	return merge_backends(smeta, backends_size, backend_params);
}

static char* convert_to_hex(void *bytes, size_t size, char *hex){
	for (int i = 0; i < size; i++){
		sprintf(hex + i * 2, "%02X", ((unsigned char*)bytes)[i]);
	}
	hex[size * 2] = NUL;
	return hex;
}

static char* check_rule(rule_t *rules, void *buffer){
	rule_t *rule = rules;
	if (strlen(rule->params.name) == 0){
		return NULL;
	}
	CONN_BUF_T(1) *conn_buffer = buffer;
	char hex[HEX_SIZE(conn_buffer->meta.bytes_received)];
	convert_to_hex(conn_buffer->data, conn_buffer->meta.bytes_received, hex);	
	for (int i = 0; i < MAX_RULES; i++){
		rule = rules + i;
		if (strlen(rule->params.name) == 0){
			break;
		}
		if (regexec(&rule->regex, hex, 0, NULL, 0) == 0){ //regexec is thread-safe
			LOG_DEBUG(SM_LOG_LEVEL, "Got match from rule %s. %ld", rule->params.name, conn_buffer->meta.bytes_received);
			return rule->params.name;
		}
	}
	LOG_DEBUG(SM_LOG_LEVEL, "Not matched. %s %ld", rule->params.name, conn_buffer->meta.bytes_received);
	return NULL;
}

char* smeta_approve(server_meta_t *smeta, void* buffer){
	return check_rule(smeta->approve_rules, buffer);
}

char* smeta_close(server_meta_t *smeta, void* buffer){
	return check_rule(smeta->close_rules, buffer);
}

char* smeta_close_client(server_meta_t *smeta, void* buffer){
	return check_rule(smeta->close_client_rules, buffer);
}

static backend_t* get_cached_backend(server_meta_t *smeta, void *buffer){
	llist_iterator_t iter;
	for (backend_t *backend = llist_iter_init(&iter, smeta->backends); backend != NULL; backend = llist_iter_next(&iter)){ 
		if (be_get_cache(backend, buffer, smeta->cache_ttl) != NULL){
			return backend;
		}
	}
	return NULL;
}

backend_t* smeta_get_cache(server_meta_t *smeta, void *buffer){
	if (smeta->cache_size <= 0){
		return NULL;
	}
	pthread_mutex_lock(&smeta->lock);
	backend_t *res = get_cached_backend(smeta, buffer);
	pthread_mutex_unlock(&smeta->lock);
	return res;
}

static backend_t* get_addr_cache(server_meta_t *smeta, void *conn){
	llist_iterator_t iter;
	for (backend_t *backend = llist_iter_init(&iter, smeta->backends); backend != NULL; backend = llist_iter_next(&iter)){ 
		if (be_conn_exist(backend, conn)){
			return backend;
		}
	}
	return NULL;
}

backend_t* smeta_get_addr_cache(server_meta_t *smeta, void *conn){
	if (!smeta->addr_cache){
		return NULL;
	}
	pthread_mutex_lock(&smeta->lock);
	backend_t *res = get_addr_cache(smeta, conn);
	pthread_mutex_unlock(&smeta->lock);
	return res;
}

uint32_t smeta_backends_size(server_meta_t *smeta){
	return smeta->backends->size;
}
		
static void* init_backend_conn(void *conn, backend_t *backend, time_t cur_time){
	CONN_CLIENT_TT(conn) *conn_client = conn;
	void *conn_backend = llist_take(&conn_client->backend_conns);
	if (conn_backend == NULL){
		LOG_ERROR(SM_LOG_LEVEL, "Can't allocate backend connection.");
		return NULL;
	}
	init_conn_backend(conn_backend, backend, conn_client, cur_time);
	if (conn_backend_connect(conn_backend) == NULL){
		free_conn_backend_data(conn_backend);
		llist_release(&conn_client->backend_conns, conn_backend);
		return NULL;
	}
	return conn;
}

static server_meta_t* init_backends(server_meta_t *smeta, void* conn, void *buffer, time_t cur_time){
	CONN_CLIENT_TT(conn) *conn_client = conn;
	backend_t *backend = NULL;
	if (smeta->cache_size > 0){
		backend = get_cached_backend(smeta, buffer);
	}
	bool addr_cache = false;
	if (backend == NULL && smeta->addr_cache){
		backend = get_addr_cache(smeta, conn);
		addr_cache = true;
	}
	if (backend != NULL){
		LOG_INFO(SM_LOG_LEVEL, "Backend connection restored from cache. Addr cache(?):%s. %p %d %s %s", (addr_cache)? "true" : "false", 
			conn_client, conn_client->base.fd, uaddr_readable(&conn_client->base.addr), backend->params.name);
		if (init_backend_conn(conn_client, backend, cur_time) == NULL){
			return NULL;
		}		
		if (!addr_cache && be_approve_conn(backend, conn_client, llist_get_first(&conn_client->backend_conns)) == NULL){
			return NULL;
		}
	} else {
		llist_iterator_t iter;
		for (backend = llist_iter_init(&iter, smeta->backends); backend != NULL; backend = llist_iter_next(&iter)){
			if (init_backend_conn(conn_client, backend, cur_time) == NULL){
				return NULL;
			}
		}
	}
	LOG_INFO(SM_LOG_LEVEL, "Client connection initialized. (%d) %p %d %s", conn_client->backend_conns.size, conn_client, conn_client->base.fd, uaddr_readable(&conn_client->base.addr));
	conn_client->initialized = true;
	return smeta;
}
		
server_meta_t* smeta_init_backends(server_meta_t *smeta, void* conn, void *buffer, time_t cur_time){
	pthread_mutex_lock(&smeta->lock);
	server_meta_t *res = init_backends(smeta, conn, buffer, cur_time);
	pthread_mutex_unlock(&smeta->lock);
	return res;
}