/** @file server_meta.h
 *  @brief mpserver metadata. List of backends to initialize a client. Helper methods to get backend from cache by buffer. Methods to approve or close connections by buffer data.
 */
#ifndef H_SERVER_META
#define H_SERVER_META

#include "../log/log.h"
#include "../config_reader/config_reader.h"
#include "../mp_config/mp_config.h"
#include "../linked_list/linked_list.h"
#include "../backend/backend.h"
#include <pthread.h>
#include <regex.h>

#ifndef BACKENDS_PREALLOC
#define BACKENDS_PREALLOC 10
#endif

typedef struct rule {
	rule_params_t params; /**< rule params */
	regex_t regex; /**< compiled regex */
} rule_t;

typedef struct server_meta {
	pthread_mutex_t lock; /**< lock to thread-safe iterate over backends list */
	llist_t *backends; /**< backends list */
	rule_t approve_rules[MAX_RULES]; /**< compiled approve backend rules */
	rule_t close_rules[MAX_RULES]; /**< compiled close backend rules */
	rule_t close_client_rules[MAX_RULES]; /**< compiled close client rules */
	uint32_t cache_ttl; /**< backend cache ttl */
	uint32_t cache_size; /**< backend cache size */
	bool addr_cache; /**< use ip address cache */
	uint32_t clients_prealloc; /**< clients_prealloc for backend initialization */
	int type; /**< backend socket type (SOCK_DGRAM, SOCK_STREAM, ...) */
	uint32_t buffer_size; /**< backend cache buffer size */
	char backends_file[MAX_BACKENDS_FILE_NAME]; /**< config file with backends */
} server_meta_t;

/**
 * @brief Initialize already allocated server_meta
 * 
 * @param smeta server meta data
 * @param reader config reader
 * @return server_meta if success, otherwise - NULL
 */
server_meta_t* init_smeta(server_meta_t *smeta, creader_t *reader);
/**
 * @brief Allocate and initialize server_meta
 * 
 * @param reader config reader
 * @return server_meta if success, otherwise - NULL
 */
server_meta_t* alloc_smeta(creader_t *reader);
/**
 * @brief Free server_meta internal structures.
 * 
 * @param smeta server meta data
 */
void free_smeta_data(server_meta_t *smeta);
/**
 * @brief Free server_meta internal structures and smeta ptr itself.
 * 
 * @param smeta server meta data
 */
void free_smeta(server_meta_t *smeta);

/**
 * @brief Load backends from file. Append new. Remove obsolete.
 * 
 * @param smeta server meta data
 * @return server_meta if success, otherwise - NULL
 */
server_meta_t* smeta_load_backends(server_meta_t *smeta);

/**
 * @brief Check approve backend connection.
 * 
 * @param smeta server meta data
 * @param buffer data buffer
 * @return server_meta if success, otherwise - NULL
 */
char* smeta_approve(server_meta_t *smeta, void* buffer);
/**
 * @brief Check close backend connection.
 * 
 * @param smeta server meta data
 * @param buffer data buffer
 * @return server_meta if success, otherwise - NULL
 */
char* smeta_close(server_meta_t *smeta, void* buffer);
/**
 * @brief Check close client connection.
 * 
 * @param smeta server meta data
 * @param buffer data buffer
 * @return server_meta if success, otherwise - NULL
 */
char* smeta_close_client(server_meta_t *smeta, void* buffer);

/**
 * @brief Get backend from cache by client data buffer.
 * 
 * @param smeta server meta data
 * @param buffer data buffer
 * @return backend if found, otherwise - NULL
 */
backend_t* smeta_get_cache(server_meta_t *smeta, void *buffer);
/**
 * @brief Get backend from addr cache by client conn addr.
 * 
 * @param smeta server meta data
 * @param conn client connection
 * @return backend if found, otherwise - NULL
 */
backend_t* smeta_get_addr_cache(server_meta_t *smeta, void *conn);

/**
 * @brief Bacends size. Helper.
 * 
 * @param smeta server meta data
 * @return backends count
 */
uint32_t smeta_backends_size(server_meta_t *smeta);

/**
 * @brief Initialize client connection by backends. Get backen from cache or copy all to newly backend connections.
 * 
 * @param smeta server meta data
 * @param conn client connection
 * @param buffer client data buffer
 * @param smeta server meta data
 * @return cur_time current time to fill new backend connection last_active
 */
server_meta_t* smeta_init_backends(server_meta_t *smeta, void *conn, void *buffer, time_t cur_time);
#endif

#ifndef SM_LOG_LEVEL
#define SM_LOG_LEVEL C_LOG_DEFAULT
#endif
