#include "server_meta.h"
#include "../backend/backend.h"

#include "server_meta.c"
#include "../config_reader/config_reader.c"
#include "../mp_config/mp_config.c"
#include "../linked_list/linked_list.c"
#include "../uni_addr/uni_addr.c"
#include "../backend/backend.c"
#include "../connection/connection.c"
#include "../connection/connection_server.c"
#include "../connection/connection_client.c"
#include "../connection/connection_backend.c"

int main(){
	creader_t *c = alloc_creader();
	cr_read_file(c, "mp_config/test.conf");
	
	server_meta_t *smeta = alloc_smeta(c);
	free_creader(c);
	printf("%d %s\n", smeta->cache_ttl, smeta->backends_file);
	
	for (int i = 0; i < MAX_RULES; i++){
		rule_t *rule = &smeta->approve_rules[i];
		if (strlen(rule->params.name) == 0){
			break;
		}
		printf("%s\n", rule->params.name);
	}

	llist_iterator_t iter;
	for (backend_t *backend = llist_iter_init(&iter, smeta->backends); backend != NULL; backend = llist_iter_next(&iter)){ 
		printf("%s %s\n", backend->params.name, backend->params.host);
	}
	
	free_smeta(smeta);
	
	return 0;
}