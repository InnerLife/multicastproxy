/** @file log.h
 *  @brief printf according to module log level
 */
#ifndef H_STDIO_EXT
#define H_STDIO_EXT

#include <stdio.h>
#include <errno.h>
#include <string.h>
#include <time.h>

#define NUL '\0'

#define C_LOG_DEBUG 0
#define C_LOG_INFO 1
#define C_LOG_ERROR 2

#ifndef C_LOG_DEFAULT
#define C_LOG_DEFAULT C_LOG_INFO
#endif

/**
 * @brief Print debug msg into stdout, if module log level matches. Always append file name.
 * 
 * @param level Module log level
 * @param format Msg format
 * @param ... Va-args
 */
#define LOG(cur_level, level, format, ...) do {\
	if (level <= cur_level) {\
		if (level <= C_LOG_DEBUG) printf("[%-30.30s] ", __FILE__);\
		printf("%.19s [%s]: "format, ctime(&(time_t){ time(0) }), (cur_level == C_LOG_DEBUG) ? "D" : ((cur_level == C_LOG_INFO) ? "I" : "E"), ##__VA_ARGS__);\
		if (level >= C_LOG_ERROR) printf(" [%d; %s]", errno, strerror(errno));\
		printf("\n");\
	}\
} while (0)

/**
 * @brief Print debug msg into stdout, if module log level matches. Always append file name.
 * 
 * @param level Module log level
 * @param format Msg format
 * @param ... Va-args
 */
#define LOG_DEBUG(level, format, ...) LOG(C_LOG_DEBUG, level, format, ##__VA_ARGS__)
/**
 * @brief Print info msg into stdout, if module log level matches. Append file name only if log level is C_LOG_DEBUG.
 * 
 * @param level Module log level
 * @param format Msg format
 * @param ... Va-args
 */
#define LOG_INFO(level, format, ...) LOG(C_LOG_INFO, level, format, ##__VA_ARGS__)
/**
 * @brief Print error msg into stderr, if module log level matches. Include errno and strerror(errno). Append file name only if log level is C_LOG_DEBUG.
 * 
 * @param level Module log level
 * @param format Msg format
 * @param ... Va-args
 */
#define LOG_ERROR(level, format, ...) LOG(C_LOG_ERROR, level, format, ##__VA_ARGS__)

#endif