#include "log.h"

int main(){
	LOG_DEBUG(C_LOG_DEBUG, "Debug. Log level %u.", C_LOG_DEBUG);
	LOG_DEBUG(C_LOG_INFO, "Debug. Log level %u.", C_LOG_INFO);
	LOG_DEBUG(C_LOG_ERROR, "Debug. Log level %u.", C_LOG_ERROR);
	
	LOG_INFO(C_LOG_DEBUG, "Info. Log level %u.", C_LOG_DEBUG);
	LOG_INFO(C_LOG_INFO, "Info. Log level %u.", C_LOG_INFO);
	LOG_INFO(C_LOG_ERROR, "Info. Log level %u.", C_LOG_ERROR);
	
	LOG_ERROR(C_LOG_DEBUG, "Error. Log level %u.", C_LOG_DEBUG);
	LOG_ERROR(C_LOG_INFO, "Error. Log level %u.", C_LOG_INFO);
	LOG_ERROR(C_LOG_ERROR, "Error. Log level %u.", C_LOG_ERROR);
	
	printf("%s", ctime(&(time_t){ time(0) }) );
	
	return 0;
}
