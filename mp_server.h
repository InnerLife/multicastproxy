/** @file mp_server.h
 *  @brief Holder of worker processes and server meta data
 */
#ifndef H_MP_SERVER
#define H_MP_SERVER

#include "log/log.h"
#include "server_meta/server_meta.h"
#include "mp_config/mp_config.h"

#include <stdbool.h>

typedef struct mp_server {
	params_t params; /**< config reader params */
	server_meta_t smeta; /**< server meta data */
	bool stopped; /**< stopping flag */
} mp_server_t;

/**
 * @brief Initialize already allocated server instance using config params from reader
 * 
 * @param mpserver allocated mp_server instance
 * @param reader config reader
 * @return server if success, otherwise - NULL
 */
mp_server_t* init_mpserver(mp_server_t *mpserver, creader_t *reader);
/**
 * @brief Allocate and initialize server instance using config params from reader
 * 
 * @param reader config reader
 * @return server if success, otherwise - NULL
 */
mp_server_t* alloc_mpserver(creader_t *reader);
/**
 * @brief Free server instance internal data structures
 * 
 * @param mpserver mp_server instance
 */
void free_mpserver_data(mp_server_t *mpserver);
/**
 * @brief Free server instance internal data structures and server ptr itself
 * 
 * @param mpserver mp_server instance
 */
void free_mpserver(mp_server_t *mpserver);

/**
 * @brief Create and loop mps_worker until mp_server is not stopped
 * 
 * @param mpserver mp_server instance
 * @return ignore
 */
void* mps_worker_thread(void *mpserver_p);
/**
 * @brief Create and loop smeta backends reload process
 * 
 * @param mpserver mp_server instance
 * @return ignore
 */
void* mps_reload_backends_thread(void *mpserver_p);

#endif

#ifndef MPS_LOG_LEVEL
#define MPS_LOG_LEVEL C_LOG_DEFAULT
#endif
