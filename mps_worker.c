#include "mps_worker.h"
#include "connection/connection_client.h"
#include "connection/connection_backend.h"

#include <stdio.h>
#include <errno.h>
#include <string.h>

#include <stdlib.h>
#include <unistd.h>
#include <sys/epoll.h>

#ifndef FAST_C2B
#define FAST_C2B false
#endif

#ifndef FAST_B2C
#define FAST_B2C false
#endif

static int const EPOLL_SIZE = 1;

static int const F_EPOLL_DEFAULT = EPOLLET;
static int const F_EPOLL_RECV = EPOLLIN | EPOLLRDNORM | EPOLLPRI | EPOLLRDBAND;
static int const F_EPOLL_SEND = EPOLLOUT | EPOLLWRNORM | EPOLLWRBAND;
static int const F_EPOLL_EXCP = EPOLLERR | EPOLLHUP | EPOLLRDHUP;
static int const F_EPOLL_RO = F_EPOLL_DEFAULT | F_EPOLL_RECV | F_EPOLL_EXCP;
static int const F_EPOLL_RW = F_EPOLL_RO | F_EPOLL_SEND;

static struct epoll_event get_epoll_event(void *conn){
	CONN_TT(conn) *conn_base = conn;
	struct epoll_event event;
	//memset(&event, 0, sizeof(event));
	if (conn_base->send_buffers.size > 0){
		event.events = F_EPOLL_RW;
	} else {
		event.events = F_EPOLL_RO;
	}
	event.data.ptr = conn_base;
	//event.data.fd = conn_base->fd; //fd or ptr, not both
	return event;
}

static void conn_add(mps_worker_t *worker, void *conn){
	CONN_TT(conn) *conn_base = conn;
	LOG_DEBUG(MPS_W_LOG_LEVEL, "Adding conn %d %p", conn_base->fd, conn_base);
	if (conn_base->fd < 0){
		return;
	}
	struct epoll_event event = get_epoll_event(conn_base);
    if (epoll_ctl(worker->epfd, EPOLL_CTL_ADD, conn_base->fd, &event) < 0 && errno == EEXIST){
		epoll_ctl(worker->epfd, EPOLL_CTL_MOD, conn_base->fd, &event);
	}
}

static void conn_mod(mps_worker_t *worker, void *conn){
	CONN_TT(conn) *conn_base = conn;
	LOG_DEBUG(MPS_W_LOG_LEVEL, "Modifying conn %d %p", conn_base->fd, conn_base);
	if (conn_base->fd < 0){
		return;
	}
	struct epoll_event event = get_epoll_event(conn_base);
	if (epoll_ctl(worker->epfd, EPOLL_CTL_MOD, conn_base->fd, &event) < 0 && errno == ENOENT){
		epoll_ctl(worker->epfd, EPOLL_CTL_ADD, conn_base->fd, &event);
	}
}
/*---------------------------------------------------------------*/
mps_worker_t* init_mps_worker(mps_worker_t *worker, uint32_t buffer_size, uint32_t max_buffers, int type, int family, short port, int backlog, server_meta_t *smeta, uint32_t clients_count, 
							uint32_t epoll_max_events, uint32_t epoll_timeout, uint32_t backend_idle_max, uint32_t client_idle_max, uint32_t idle_cleaner_sleep){
	worker->epfd = epoll_create(EPOLL_SIZE);
	if (worker->epfd < 0){
		LOG_ERROR(MPS_W_LOG_LEVEL, "Can not init worker. Error creating epoll.");
		return NULL;
	}	
	worker->events = malloc(sizeof(*worker->events) * epoll_max_events);
	if (worker->events == NULL) {
		LOG_ERROR(MPS_W_LOG_LEVEL, "Can not init worker. Can not allocate events.");		
		close(worker->epfd);
		return NULL;
	}
	worker->conn_server = alloc_conn_server(
		buffer_size,
		max_buffers,
		type,
		family,
		port,
		backlog,
		smeta,
		clients_count
	);
	if (worker->conn_server == NULL){
		LOG_ERROR(MPS_W_LOG_LEVEL, "Can not init worker. Can not create server.");
		close(worker->epfd);
		free(worker->events);
		return NULL;
	}
	if (conn_server_open(worker->conn_server) == NULL) {
		LOG_ERROR(MPS_W_LOG_LEVEL, "Can not init worker. Can not open server.");		
		free_conn_server(worker->conn_server);
		close(worker->epfd);
		free(worker->events);
		return NULL;
	}
	
	conn_add(worker, worker->conn_server);
	worker->epoll_max_events = epoll_max_events;
	worker->epoll_timeout = epoll_timeout;
	worker->last_cleaned = time(0);
	worker->backend_idle_max = backend_idle_max;
	worker->client_idle_max = client_idle_max;
	worker->idle_cleaner_sleep = idle_cleaner_sleep;
	return worker;
}

mps_worker_t* alloc_mps_worker(uint32_t buffer_size, uint32_t max_buffers, int type, int family, short port, int backlog, server_meta_t *smeta, uint32_t clients_count, 
								uint32_t epoll_max_events, uint32_t epoll_timeout, uint32_t backend_idle_max, uint32_t client_idle_max, uint32_t idle_cleaner_sleep){
	mps_worker_t *worker = malloc(sizeof(mps_worker_t));
	if (worker == NULL){
		LOG_ERROR(MPS_W_LOG_LEVEL, "Error allocating mps_worker.");
		return NULL;
	}
	LOG_DEBUG(MPS_W_LOG_LEVEL, "mps_worker allocated. %p", worker);
	if (init_mps_worker(worker, buffer_size, max_buffers, type, family, port, backlog, smeta, clients_count, 
							epoll_max_events, epoll_timeout, backend_idle_max, client_idle_max, idle_cleaner_sleep) == NULL){
		return NULL;
	}
	return worker;
}

void free_mps_worker_data(mps_worker_t *worker){
	conn_close_server(worker->conn_server);
	free_conn_server(worker->conn_server);
	close(worker->epfd);
	free(worker->events);
	LOG_DEBUG(MPS_W_LOG_LEVEL, "mps_worker data freed. %p", worker);
}

void free_mps_worker(mps_worker_t *worker){
	free_mps_worker_data(worker);
	free(worker);
	LOG_DEBUG(MPS_W_LOG_LEVEL, "mps_worker freed. %p", worker);
}
/*---------------------------------------------------------------*/

static void remove_conn_backend(void *conn){
	CONN_BACKEND_TT(conn) *conn_backend = conn;
	CONN_CLIENT_TT(conn_backend->conn_client) *conn_client = (void*)conn_backend->conn_client;
	
	conn_close_backend(conn_backend);
	conn_client_remove_backend(conn_client, conn_backend);
}

static bool check_remove_conn_client(void *conn){
	CONN_CLIENT_TT(conn) *conn_client = conn;
	if (conn_client->backend_conns.size == 0){
		conn_close_client(conn_client);
		conn_server_remove_client(conn_client->conn_server, conn_client);
		return true;
	}
	return false;
}

static void remove_conn_client(void *conn){
	CONN_CLIENT_TT(conn) *conn_client = conn;
	
	llist_iterator_t iter;
	for (void *conn_backend = llist_iter_init(&iter, &conn_client->backend_conns); conn_backend != NULL; conn_backend = llist_iter_next(&iter)){
		remove_conn_backend(conn_backend);
	}
	check_remove_conn_client(conn_client);
}

static void clean_conns(mps_worker_t *worker, time_t cur_time){
	LOG_DEBUG(MPS_W_LOG_LEVEL, "Cleaner started. %p", worker);
	uint32_t buffer_size = CONN_BUFFER_SIZE(worker->conn_server);
	CONN_SERVER_T(buffer_size) *conn_server = worker->conn_server;
	llist_iterator_t iter_c;
	llist_iterator_t iter_b;
	for (CONN_CLIENT_T(buffer_size) *conn_client = llist_iter_init(&iter_c, conn_server->client_conns); conn_client != NULL; conn_client = llist_iter_next(&iter_c)){
		for (CONN_BACKEND_T(buffer_size) *conn_backend = llist_iter_init(&iter_b, &conn_client->backend_conns); conn_backend != NULL; conn_backend = llist_iter_next(&iter_b)){
			//LOG_INFO(MPS_W_LOG_LEVEL, "%lld %lld %d %d %d", (long long)conn_backend->base.last_active, (long long)cur_time, worker->client_idle_max, worker->backend_idle_max, conn_backend->base.fd);
			if (conn_backend->base.fd == -1){
				LOG_INFO(MPS_W_LOG_LEVEL, "Removing closed backend connection... %p %d %s", conn_backend, conn_backend->base.fd, uaddr_readable(&conn_backend->base.addr));
			} else if (conn_backend->base.last_active + worker->client_idle_max < cur_time){
				LOG_INFO(MPS_W_LOG_LEVEL, "Backend conn marked as idle. Removing... %p %d %s", conn_backend, conn_backend->base.fd, uaddr_readable(&conn_backend->base.addr));
			} else {
				CONN_BUF_T(buffer_size) *buf = llist_get_first(&conn_backend->base.send_buffers);
				if (buf != NULL && buf->meta.time_received + worker->backend_idle_max < cur_time){
					LOG_INFO(MPS_W_LOG_LEVEL, "Backend conn buffer marked as idle. Removing... %p %d %s", conn_backend, conn_backend->base.fd, uaddr_readable(&conn_backend->base.addr));
				} else {
					continue;
				}
			}
			remove_conn_backend(conn_backend);
		}
		if (conn_client->initialized && check_remove_conn_client(conn_client)){
			continue;
		}
		if (conn_client->base.fd == -1){
			LOG_INFO(MPS_W_LOG_LEVEL, "Removing closed client connection... %p %d %s", conn_client, conn_client->base.fd, uaddr_readable(&conn_client->base.addr));
		} else if (conn_client->base.last_active + worker->client_idle_max < cur_time){
			LOG_INFO(MPS_W_LOG_LEVEL, "Client connection marked as idle. Removing... %p %d %s", conn_client, conn_client->base.fd, uaddr_readable(&conn_client->base.addr));
		} else {
			continue;
		}
		remove_conn_client(conn_client);
	}
	LOG_DEBUG(MPS_W_LOG_LEVEL, "Cleaner stopped. %p", worker);
}


static void* rwx_conn_server(mps_worker_t *worker, time_t cur_time, uint32_t events, void *conn){
	CONN_SERVER_TT(conn) *conn_server = conn;
	if ((events & F_EPOLL_EXCP) != 0){
		LOG_INFO(MPS_W_LOG_LEVEL, "Server conn closed by event. %p %d %s", conn_server, conn_server->base.fd, uaddr_readable(&conn_server->base.addr));
		return NULL;
	}
	if ((events & F_EPOLL_RECV) != 0) {
		void *new_client = NULL;
		if (conn_server_accept(conn_server, cur_time, &new_client) == NULL){
			return NULL;
		}
		if (new_client != NULL){
			if (conn_server->type == SOCK_DGRAM){
				conn_add(worker, conn_server);
				conn_mod(worker, new_client);
				return conn_server;
			} else {
				conn_add(worker, new_client);
			}
		}
	}
	conn_mod(worker, conn_server);
	return conn_server;
}

static void* process_conn_server(mps_worker_t *worker, time_t cur_time, uint32_t events, void *conn){
	CONN_SERVER_TT(conn) *conn_server = conn;
	if (conn_server->base.fd == -1){
		LOG_INFO(MPS_W_LOG_LEVEL, "Server conn was closed.");
		return NULL;
	}
	if (rwx_conn_server(worker, cur_time, events, conn_server) == NULL){
		return NULL;
	}
	return conn_server;
}

static void* rwx_conn_client(mps_worker_t *worker, time_t cur_time, uint32_t events, void *conn){
	uint32_t buffer_size = CONN_BUFFER_SIZE(conn);
	CONN_CLIENT_T(buffer_size) *conn_client = conn;
	if ((events & F_EPOLL_EXCP) != 0){
		LOG_INFO(MPS_W_LOG_LEVEL, "Client conn closed by event. %p %d %s", conn_client, conn_client->base.fd, uaddr_readable(&conn_client->base.addr));
		return NULL;
	}
	if ((events & F_EPOLL_RECV) != 0) {
		bool initialized = false;
		bool moved = false;
		if (conn_client_recv(conn_client, cur_time, &initialized, &moved) == NULL){
			return NULL;
		}
		if (moved){
			llist_iterator_t iter;
			for (CONN_BACKEND_T(buffer_size) *conn_backend = llist_iter_init(&iter, &conn_client->backend_conns); conn_backend != NULL; conn_backend = llist_iter_next(&iter)){
				//---------
				bool remove_client = false;
				if (FAST_C2B && conn_backend_send(conn_backend, cur_time, &remove_client, &moved) != NULL && conn_backend->base.send_buffers.size == 0) {
					if (initialized){
						conn_add(worker, conn_backend);
					}
					continue;
				}
				//---------
				if (initialized){
					conn_add(worker, conn_backend);
				} else {
					conn_mod(worker, conn_backend);
				}
			}
		}
	}
	if ((events & F_EPOLL_SEND) != 0 && conn_client_send(conn_client, cur_time) == NULL) {
		return NULL;
	}
	conn_mod(worker, conn_client);
	return conn_client;
}

static void* process_conn_client(mps_worker_t *worker, time_t cur_time, uint32_t events, void *conn){
	CONN_CLIENT_TT(conn) *conn_client = conn;
	if (conn_client->base.fd == -1){
		LOG_INFO(MPS_W_LOG_LEVEL, "Client conn was closed internally. %p %d %s", conn_client, conn_client->base.fd, uaddr_readable(&conn_client->base.addr));
		return NULL;
	} else if (conn_client->initialized && conn_client->backend_conns.size == 0){
		LOG_INFO(MPS_W_LOG_LEVEL, "Client conn has no backend conns. Removing... %p %d %s", conn_client, conn_client->base.fd, uaddr_readable(&conn_client->base.addr));
		return NULL;
	} else if (rwx_conn_client(worker, cur_time, events, conn_client) == NULL){
		return NULL;		
	}
	return conn_client;
}

static void* rwx_conn_backend(mps_worker_t *worker, time_t cur_time, uint32_t events, void *conn, bool *remove_client){
	CONN_BACKEND_TT(conn) *conn_backend = conn;
	if ((events & F_EPOLL_EXCP) != 0){
		LOG_INFO(MPS_W_LOG_LEVEL, "Backend conn closed by event. %p %d %s", conn_backend, conn_backend->base.fd, uaddr_readable(&conn_backend->base.addr));
		return NULL;
	}
	if ((events & F_EPOLL_RECV) != 0) {
		bool moved = false;
		void *res = conn_backend_recv(conn_backend, cur_time, remove_client, &moved);
		if (moved){
			conn_mod(worker, conn_backend->conn_client);
		}
		if (res == NULL){
			return NULL;
		}
	}
	if ((events & F_EPOLL_SEND) != 0) {
		bool moved = false;
		void *res = conn_backend_send(conn_backend, cur_time, remove_client, &moved);
		//---------
		if (moved && FAST_B2C && conn_client_send(conn_backend->conn_client, cur_time) != NULL && conn_backend->conn_client->base.send_buffers.size == 0){
			moved = false;
		}
		//---------
		if (moved){
			conn_mod(worker, conn_backend->conn_client);
		}
		if (res == NULL){
			return NULL;
		}
	}	
	conn_mod(worker, conn_backend);
	return conn_backend;
}

static void* process_conn_backend(mps_worker_t *worker, time_t cur_time, uint32_t events, void *conn, bool *remove_client){
	CONN_BACKEND_TT(conn) *conn_backend = conn;
	if (conn_backend->base.fd == -1){
		LOG_INFO(MPS_W_LOG_LEVEL, "Backend conn was closed internally. %p %d %s", conn_backend, conn_backend->base.fd, uaddr_readable(&conn_backend->base.addr));
		return NULL;
	} else if (conn_backend->backend->closed){
		LOG_INFO(MPS_W_LOG_LEVEL, "Removing backend conn as backend was closed... %p %d %s", conn_backend, conn_backend->base.fd, uaddr_readable(&conn_backend->base.addr));
		return NULL;
	} else if (rwx_conn_backend(worker, cur_time, events, conn_backend, remove_client) == NULL){
		return NULL;
	}
	return conn_backend;
}

static void clean_events(mps_worker_t *worker, int count, int begin, void *conn){
	for (int i = begin; i < count; i++){
		struct epoll_event *event = worker->events + i;
		if (event->data.ptr == NULL){
			continue;
		}
		if (event->data.ptr == conn){
			event->data.ptr = NULL;
		}
	}
}

static void clean_client_events(mps_worker_t *worker, int count, int begin, void *conn){
	CONN_CLIENT_TT(conn) *conn_client = conn;
	llist_iterator_t iter;
	for (void *conn_backend = llist_iter_init(&iter, &conn_client->backend_conns); conn_backend != NULL; conn_backend = llist_iter_next(&iter)){
		clean_events(worker, count, begin, conn_backend);
	}
	clean_events(worker, count, begin, conn_client);
}

mps_worker_t* mps_worker_poll(mps_worker_t *worker){
	int events_count = epoll_wait(
		worker->epfd, 
		worker->events, 
		worker->epoll_max_events, 
		worker->epoll_timeout
	);
	if (events_count < 0 && errno != EINTR){
		LOG_ERROR(MPS_W_LOG_LEVEL, "Epoll wait error.");
		return NULL;
	}
	//LOG_DEBUG(MPS_W_LOG_LEVEL, "Got events. %d", events_count);
	time_t cur_time = time(0);
	for (int i = 0; i < events_count; i++){
		struct epoll_event *event = worker->events + i;
		if (event->data.ptr == NULL){
			continue;
		}
		
		CONN_TT(event->data.ptr) *conn_base = event->data.ptr;
		LOG_DEBUG(MPS_W_LOG_LEVEL, "Processing event. %p %d %d", conn_base, conn_base->fd, event->events);

		if (conn_base->conn_type == CONN_TYPE_BACKEND) {
			bool remove_client = false;
			CONN_BACKEND_TT(conn_base) *conn_backend = (void*)conn_base;
			void *res = process_conn_backend(worker, cur_time, event->events, conn_base, &remove_client);
			void *conn_client = conn_backend->conn_client;
			if (remove_client){
				clean_client_events(worker, events_count, i + 1, conn_client);
				remove_conn_client(conn_client);
			} else if (res == NULL){
				clean_events(worker, events_count, i + 1, conn_backend);
				remove_conn_backend(conn_backend);
				if (check_remove_conn_client(conn_client)){
					clean_events(worker, events_count, i + 1, conn_client);
				}
			}
		} else if (conn_base->conn_type == CONN_TYPE_CLIENT) {
			if (process_conn_client(worker, cur_time, event->events, conn_base) == NULL){
				clean_client_events(worker, events_count, i + 1, conn_base);
				remove_conn_client(conn_base);
			}
		} else {
			if (process_conn_server(worker, cur_time, event->events, conn_base) == NULL){
				return NULL;
			}
		}
	}
	
	if (worker->last_cleaned + worker->idle_cleaner_sleep < cur_time){
		clean_conns(worker, cur_time);
		worker->last_cleaned = cur_time;
	}
	
	return worker;
}