#include "mp_config.h"

#include <stdlib.h>
#include <sys/socket.h>
#include <regex.h>
#include <limits.h>

static short get_type(creader_t *reader, char *group, char *key, int *type){
	char *str_type = cr_get_value(reader, group, key);
	if (str_type == NULL){
		LOG_ERROR(MPC_LOG_LEVEL, "Error reading socket type. %s %s", group, key);
		return 1;
	}
	if (strcmp(str_type, TYPE_TCP) == 0){
		*type = SOCK_STREAM;
		return 0;
	} else if (strcmp(str_type, TYPE_UDP) == 0){
		*type = SOCK_DGRAM;
		return 0;
	}
	LOG_ERROR(MPC_LOG_LEVEL, "Error reading socket type. %s %s", group, key);
	return 1;
}

static short get_family(creader_t *reader, char *group, char *key, int *family){
	char *str_family = cr_get_value(reader, group, key);
	if (str_family == NULL){
		LOG_ERROR(MPC_LOG_LEVEL, "Error reading socket family. %s %s", group, key);
		return 1;
	}
	if (strcmp(str_family, FAMILY_V6) == 0){
		*family = AF_INET6;
		return 0;
	} else if (strcmp(str_family, FAMILY_V4) == 0){
		*family = AF_INET;
		return 0;
	}
	LOG_ERROR(MPC_LOG_LEVEL, "Error reading socket family. %s %s", group, key);
	return 1;
}

static short get_int_param(creader_t *reader, char *group, char *key, int min_value, int default_value, uint32_t *param){
	char *str_value = cr_get_value(reader, group, key);
	if (str_value == NULL){
		if (default_value == INT_MIN){
			LOG_ERROR(MPC_LOG_LEVEL, "Error reading mandatory param. %s %s", group, key);
			return 1;
		} else {
			*param = default_value;
			LOG_DEBUG(MPC_LOG_LEVEL, "Param read from defaults. %s %s %d", group, key, default_value);
			return 0;
		}
	}
	int value = atoi(str_value);
	if (value < min_value){
		LOG_ERROR(MPC_LOG_LEVEL, "Param is less than %d. %s %s %s", min_value, group, key, str_value);
		return 1;
	}	
	*param = value;
	LOG_DEBUG(MPC_LOG_LEVEL, "Param read from config. %s %s %s", group, key, str_value);
	return 0;
}

static short get_str_param(creader_t *reader, char *group, char *key, int max_size, char *param){
	char *str_value = cr_get_value(reader, group, key);
	if (str_value == NULL){
		LOG_ERROR(MPC_LOG_LEVEL, "Error reading param. %s %s", group, key);
		return 1;
	}
	strncpy(param, str_value, max_size - 1);
	param[max_size - 1] = NUL;
	return 0;
}

params_t* mpc_get_params(creader_t *reader){
	static params_t params;
	uint32_t errorCount = 0;
	errorCount += get_type(reader, GROUP_SERVER, PARAM_TYPE, &params.type);
	errorCount += get_family(reader, GROUP_SERVER, PARAM_FAMILY, &params.family);
	errorCount += get_int_param(reader, GROUP_SERVER, PARAM_PORT, MIN_PORT, INT_MIN, (uint32_t*)&params.port);
	errorCount += get_int_param(reader, GROUP_SERVER, PARAM_BUFFER_SIZE, MIN_BUFFER_SIZE, DEFAULT_BUFFER_SIZE, &params.buffer_size);
	errorCount += get_int_param(reader, GROUP_SERVER, PARAM_MAX_BUFFERS, MIN_MAX_BUFFERS, DEFAULT_MAX_BUFFERS, &params.max_buffers);
	errorCount += get_int_param(reader, GROUP_SERVER, PARAM_CACHE_TTL, MIN_CACHE_TTL, DEFAULT_CACHE_TTL, &params.cache_ttl);
	errorCount += get_int_param(reader, GROUP_SERVER, PARAM_CACHE_SIZE, MIN_CACHE_SIZE, DEFAULT_CACHE_SIZE, &params.cache_size);
	errorCount += get_int_param(reader, GROUP_SERVER, PARAM_ADDR_CACHE, MIN_ADDR_CACHE, DEFAULT_ADDR_CACHE, (uint32_t*)&params.addr_cache);
	errorCount += get_int_param(reader, GROUP_SERVER, PARAM_BACKLOG, MIN_BACKLOG, DEFAULT_BACKLOG, &params.backlog);
	errorCount += get_int_param(reader, GROUP_SERVER, PARAM_WORKERS, MIN_WORKERS, DEFAULT_WORKERS, &params.workers);
	errorCount += get_int_param(reader, GROUP_SERVER, PARAM_IDLE_CLEANER_SLEEP, MIN_IDLE_CLEANER_SLEEP, DEFAULT_IDLE_CLEANER_SLEEP, &params.idle_cleaner_sleep);
	errorCount += get_int_param(reader, GROUP_SERVER, PARAM_BACKEND_IDLE_MAX, MIN_BACKEND_IDLE_MAX, DEFAULT_BACKEND_IDLE_MAX, &params.backend_idle_max);
	errorCount += get_int_param(reader, GROUP_SERVER, PARAM_CLIENT_IDLE_MAX, MIN_CLIENT_IDLE_MAX, DEFAULT_CLIENT_IDLE_MAX, &params.client_idle_max);
	errorCount += get_str_param(reader, GROUP_SERVER, PARAM_BACKENDS_FILE, MAX_BACKENDS_FILE_NAME, params.backends_file);
	errorCount += get_int_param(reader, GROUP_SERVER, PARAM_BACKENDS_RELOAD_SLEEP, MIN_BACKENDS_RELOAD_SLEEP, DEFAULT_BACKENDS_RELOAD_SLEEP, &params.backends_reload_sleep);
	errorCount += get_int_param(reader, GROUP_SERVER, PARAM_EPOLL_MAX_EVENTS, MIN_EPOLL_MAX_EVENTS, DEFAULT_EPOLL_MAX_EVENTS, &params.epoll_max_events);
	errorCount += get_int_param(reader, GROUP_SERVER, PARAM_EPOLL_TIMEOUT, MIN_EPOLL_TIMEOUT, DEFAULT_EPOLL_TIMEOUT, &params.epoll_timeout);
	errorCount += get_int_param(reader, GROUP_SERVER, PARAM_CLIENTS_PREALLOC, MIN_CLIENTS_PREALLOC, DEFAULT_CLIENTS_PREALLOC, &params.clients_prealloc);
	if (errorCount > 0){
		LOG_ERROR(MPC_LOG_LEVEL, "Error reading config params.");
		return NULL;
	}
	LOG_INFO(MPC_LOG_LEVEL, "Config params read successfully.");
	return &params;
}

static int64_t get_rules(creader_t *reader, rule_params_t *out_rules[], char *group){
	static rule_params_t rules[MAX_RULES];
	memset(&rules, 0, sizeof(rules));
	uint32_t count = cr_get_count(reader, group, false);
	if (count == 0){
		return 0;
	}
	if (count > MAX_RULES){
		LOG_INFO(MPC_LOG_LEVEL, "Max rules count exceeded. Continuing with first %u rules...", MAX_RULES);
		count = MAX_RULES;
	}
	uint32_t counter = 0;
	cr_iterator_t iter;
	for (cr_line_t *l = cr_init_iter(&iter, reader, group, false); l != NULL; l = cr_iter_next(&iter)){
		rule_params_t *rule = &rules[counter++];
		strncpy(rule->name, l->key, MAX_GROUP_NAME);
		rule->name[MAX_GROUP_NAME - 1] = NUL;
		strncpy(rule->regex, l->value, MAX_REGEX);
		rule->regex[MAX_REGEX - 1] = NUL;
		
		regex_t regex;
		if (regcomp(&regex, rule->regex, REG_EXTENDED) != 0){
			LOG_ERROR(MPC_LOG_LEVEL, "Can not compile rule. %s %s %s", l->group, l->key, l->value);
			return -1;
		}
		regfree(&regex);
		LOG_DEBUG(MPC_LOG_LEVEL, "Rule compiled. %s %s %s", l->group, l->key, l->value);
	}
	*out_rules = &rules[0];
	return counter;
}

int64_t mpc_get_approve_rules(creader_t *reader, rule_params_t *rules[]){
	return get_rules(reader, rules, GROUP_APPROVE_RULES);
}
int64_t mpc_get_close_rules(creader_t *reader, rule_params_t *rules[]){
	return get_rules(reader, rules, GROUP_CLOSE_RULES);
}
int64_t mpc_get_close_client_rules(creader_t *reader, rule_params_t *rules[]){
	return get_rules(reader, rules, GROUP_CLOSE_CLIENT_RULES);
}

static backend_params_t* get_backend(creader_t *reader, backend_params_t *backend){
	uint32_t errorCount = 0;
	errorCount += get_int_param(reader, backend->name, PARAM_PORT, MIN_PORT, INT_MIN, (uint32_t*)&backend->port);
	errorCount += get_str_param(reader, backend->name, PARAM_HOST, MAX_BACKEND_ADDR, backend->host);
	errorCount += get_int_param(reader, backend->name, PARAM_MAX_APPROVED_ADDRS, MIN_MAX_ADDRS, DEFAULT_MAX_ADDRS, &backend->max_approved_addrs);
	errorCount += get_int_param(reader, backend->name, PARAM_MAX_APPROVED_THREADS, MIN_MAX_THREADS, DEFAULT_MAX_THREADS, &backend->max_approved_threads);
	errorCount += get_int_param(reader, backend->name, PARAM_MAX_APPROVED_ADDR_THREADS, MIN_MAX_ADDR_THREADS, DEFAULT_MAX_ADDR_THREADS, &backend->max_approved_addr_threads);
	errorCount += get_int_param(reader, backend->name, PARAM_MAX_ADDRS, MIN_MAX_ADDRS, DEFAULT_MAX_ADDRS, &backend->max_addrs);
	errorCount += get_int_param(reader, backend->name, PARAM_MAX_THREADS, MIN_MAX_THREADS, DEFAULT_MAX_THREADS, &backend->max_threads);
	errorCount += get_int_param(reader, backend->name, PARAM_MAX_ADDR_THREADS, MIN_MAX_ADDR_THREADS, DEFAULT_MAX_ADDR_THREADS, &backend->max_addr_threads);
	if (errorCount > 0){
		LOG_ERROR(MPC_LOG_LEVEL, "Error reading backend params. %s", backend->name);
		return NULL;
	}
	LOG_DEBUG(MPC_LOG_LEVEL, "Backend params read successfully. %s", backend->name);
	return backend;
}

int64_t mpc_get_backends(creader_t *reader, backend_params_t *out_backends[]){
	static backend_params_t backends[MAX_BACKENDS];
	uint32_t count = cr_get_count(reader, NULL, true);
	if (count == 0){
		LOG_ERROR(MPC_LOG_LEVEL, "Backends file is empty.");
		return -1;
	}
	if (count > MAX_BACKENDS){
		LOG_INFO(MPC_LOG_LEVEL, "Max backends count exceeded. Continuing with first %u backends...", MAX_BACKENDS);
		count = MAX_BACKENDS;
	}
	uint32_t counter = 0;
	cr_iterator_t iter;
	for (cr_line_t *l = cr_init_iter(&iter, reader, NULL, true); l != NULL; l = cr_iter_next(&iter)){
		backend_params_t *backend = &backends[counter++];
		strncpy(backend->name, l->group, MAX_GROUP_NAME);
		backend->name[MAX_GROUP_NAME - 1] = NUL;
		if (get_backend(reader, backend) == NULL) {
			return -1;
		}
	}
	*out_backends = &backends[0];
	return counter;
}