/** @file mp_config.h
 *  @brief mp_server config. Read params from file to struct using config reader.
 */
#ifndef H_MP_CONFIG
#define H_MP_CONFIG

#include "../log/log.h"
#include "../config_reader/config_reader.h"

#define MAX_BACKENDS_FILE_NAME 1024
#define MAX_GROUP_NAME 128
#define MAX_BACKEND_ADDR 128
#define MAX_REGEX 2048
#define MAX_RULES 64
#define MAX_BACKENDS 1024

//known config file groups
#define GROUP_SERVER "SERVER"
#define GROUP_APPROVE_RULES "APPROVE_RULES"
#define GROUP_CLOSE_RULES "CLOSE_RULES"
#define GROUP_CLOSE_CLIENT_RULES "CLOSE_CLIENT_RULES"

//known param names
#define PARAM_TYPE "type"
#define PARAM_FAMILY "family"
#define PARAM_PORT "port"
#define PARAM_BUFFER_SIZE "buffer_size"
#define PARAM_MAX_BUFFERS "max_buffers"
#define PARAM_CACHE_TTL "cache_ttl"
#define PARAM_CACHE_SIZE "cache_size"
#define PARAM_ADDR_CACHE "addr_cache"
#define PARAM_BACKLOG "backlog"
#define PARAM_WORKERS "workers"
#define PARAM_IDLE_CLEANER_SLEEP "idle_cleaner_sleep"
#define PARAM_BACKEND_IDLE_MAX "backend_idle_max"
#define PARAM_CLIENT_IDLE_MAX "client_idle_max"
#define PARAM_BACKENDS_FILE "backends_file"
#define PARAM_BACKENDS_RELOAD_SLEEP "backends_reload_sleep"
#define PARAM_EPOLL_MAX_EVENTS "epoll_max_events"
#define PARAM_EPOLL_TIMEOUT "epoll_timeout"
#define PARAM_CLIENTS_PREALLOC "clients_prealloc"
#define PARAM_HOST "host"
#define PARAM_MAX_APPROVED_ADDRS "max_approved_addrs"
#define PARAM_MAX_APPROVED_THREADS "max_approved_threads"
#define PARAM_MAX_APPROVED_ADDR_THREADS "max_approved_addr_threads"
#define PARAM_MAX_ADDRS "max_addrs"
#define PARAM_MAX_THREADS "max_threads"
#define PARAM_MAX_ADDR_THREADS "max_addr_threads"

//min params values
#define MIN_PORT 0
#define MIN_BUFFER_SIZE 1024
#define MIN_MAX_BUFFERS 2
#define MIN_CACHE_TTL 1
#define MIN_CACHE_SIZE 0
#define MIN_ADDR_CACHE 0
#define MIN_BACKLOG 0
#define MIN_WORKERS 1
#define MIN_IDLE_CLEANER_SLEEP 1
#define MIN_BACKEND_IDLE_MAX 1
#define MIN_CLIENT_IDLE_MAX 1
#define MIN_BACKENDS_RELOAD_SLEEP 1
#define MIN_EPOLL_MAX_EVENTS 1
#define MIN_EPOLL_TIMEOUT -1
#define MIN_CLIENTS_PREALLOC 10
#define MIN_MAX_ADDRS 0
#define MIN_MAX_THREADS 0
#define MIN_MAX_ADDR_THREADS 0

#define DEFAULT_BUFFER_SIZE 4096
#define DEFAULT_MAX_BUFFERS 4
#define DEFAULT_CACHE_TTL 30
#define DEFAULT_CACHE_SIZE 0
#define DEFAULT_ADDR_CACHE 0
#define DEFAULT_BACKLOG 1
#define DEFAULT_WORKERS 1
#define DEFAULT_IDLE_CLEANER_SLEEP 1
#define DEFAULT_BACKEND_IDLE_MAX 3
#define DEFAULT_CLIENT_IDLE_MAX 30
#define DEFAULT_BACKENDS_RELOAD_SLEEP 3
#define DEFAULT_EPOLL_MAX_EVENTS 100
#define DEFAULT_EPOLL_TIMEOUT 100
#define DEFAULT_CLIENTS_PREALLOC 100
#define DEFAULT_MAX_ADDRS 0
#define DEFAULT_MAX_THREADS 0
#define DEFAULT_MAX_ADDR_THREADS 0

//supported socket types
#define TYPE_TCP "TCP"
#define TYPE_UDP "UDP"

//supported socket families
#define FAMILY_V4 "IPv4"
#define FAMILY_V6 "IPv6"

typedef struct params {
	int type; /**< socket type (SOCK_DGRAM, SOCK_STREAM, ...) */
	int family; /**< socket family (AF_INET, AF_INET6, ...) */
	unsigned short port; /**< server port */
	uint32_t buffer_size; /**< server buffer size */
	uint32_t max_buffers; /**< max buffers(send+recv) per connection */
	uint32_t cache_ttl; /**< backend cache ttl */
	uint32_t cache_size; /**< backend cache size */
	bool addr_cache; /**< use ip address cache */
	uint32_t backlog; /**< server tcp backlog */
	uint32_t workers; /**< number of workers */
	uint32_t idle_cleaner_sleep; /**< call cleaner not more often than idle_cleaner_sleep; seconds */
	uint32_t backend_idle_max; /**< max time backend connection send buffer can be idle until cleaned; seconds */
	uint32_t client_idle_max; /**< max time client and backend connection can be idle until cleaned; seconds */
	char backends_file[MAX_BACKENDS_FILE_NAME]; /**< file containing backends config */
	uint32_t backends_reload_sleep; /**< reload backends from file each backends_reload_sleep seconds */
	uint32_t epoll_max_events; /**< epoll_wait max events */
	uint32_t epoll_timeout; /**< epoll_wait timeout; milliseconds */
	uint32_t clients_prealloc; /**< target number of clients for server, number of client to prealloc; each client above this number will be allocated directly using malloc */
} params_t;


typedef struct rule_params {
	char name[MAX_GROUP_NAME]; /**< rule name */
	char regex[MAX_REGEX]; /**< regex str as is */
} rule_params_t;

typedef struct backend_params {
	char name[MAX_GROUP_NAME]; /**< backend name */
	unsigned short port; /**< backend port */
	char host[MAX_BACKEND_ADDR]; /**< backend address(ip or name) */
	uint32_t max_approved_addrs; /**< max distinct approved client ip count per backend */
	uint32_t max_approved_threads; /**< max distinct approved client threads count per backend */
	uint32_t max_approved_addr_threads; /**< max distinct approved client threads count per client ip */
	uint32_t max_addrs; /**< max distinct unapproved client ip count per backend */
	uint32_t max_threads; /**< max distinct unapproved client threads count per backend */
	uint32_t max_addr_threads; /**< max distinct unapproved client threads count per client ip */
} backend_params_t;

/**
 * @brief Get params from config reader
 * 
 * @param reader config reader
 * @return params if success, otherwise - NULL
 */
params_t* mpc_get_params(creader_t *reader);
/**
 * @brief Get approve backend rules.
 * 
 * @param reader config reader
 * @param rules array of rules
 * @return count of rules, -1 - error compiling rule
 */
int64_t mpc_get_approve_rules(creader_t *reader, rule_params_t *rules[]);
/**
 * @brief Get close backend rules.
 * 
 * @param reader config reader
 * @param rules array of rules
 * @return count of rules, -1 - error compiling rule
 */
int64_t mpc_get_close_rules(creader_t *reader, rule_params_t *rules[]);
/**
 * @brief Get close client rules.
 * 
 * @param reader config reader
 * @param rules array of rules
 * @return count of rules, -1 - error compiling rule
 */
int64_t mpc_get_close_client_rules(creader_t *reader, rule_params_t *rules[]);
/**
 * @brief Get backends from reader opened backends file. 
 * 
 * @param reader config reader
 * @param out_backends array of backends
 * @return count of rules, -1 - parsing error
 */
int64_t mpc_get_backends(creader_t *reader, backend_params_t *out_backends[]);
#endif

#ifndef MPC_LOG_LEVEL 
#define MPC_LOG_LEVEL C_LOG_DEFAULT
#endif