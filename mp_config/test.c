#include "mp_config.h"

#include "mp_config.c"
#include "../config_reader/config_reader.c"

int main(){
	creader_t *c = alloc_creader();
	cr_read_file(c, "mp_config/test.conf");
	
	params_t *p = mpc_get_params(c);
	printf("%ul %ul %s\n", p->buffer_size, p->cache_ttl, p->backends_file);
	
	rule_params_t *rs;
	int count = mpc_get_approve_rules(c, &rs);
	printf("%i\n", count);
	for (int i = 0; i < count; i++){
		rule_params_t *r = rs + i;
		printf("%s %s\n", r->name, r->regex);
	}
	
	free_creader(c);
	
	c = alloc_creader();
	cr_read_file(c, "mp_config/test.backends.conf");
	backend_params_t *bs;
	count = mpc_get_backends(c, &bs);
	for (int i = 0; i < count; i++){
		backend_params_t *b = bs + i;
		printf("%s %s\n", b->name, b->host);
	}
	free_creader(c);
	
	return 0;
}