#include "config_reader.h"

#include "config_reader.c"

typedef struct test {
	uint64_t i1; /**< char array read from config file */
	uint64_t i2; /**< parsed config lines */
	uint64_t i3; /**< count of config lines */
} test_t;

int main(){
	/*
	test_t *t = malloc(sizeof(*t) * 3);
	test_t *t2 = t + 2;
	t2->i1 = 1; t2->i2 = 2; t2->i3 = 3; 
	printf("%lu %lu %lu\n", t2->i1, t2->i2, t2->i3);
	
	printf("%p\n", t2);
	test_t *n = malloc(sizeof(*n) * 3);
	//n[1] = t2;
	memcpy(n + 2, t2, sizeof(*t2));
	//printf("%p\n", n[1]);
	test_t *n2 = n + 2;
	printf("%lu %lu %lu\n", n2->i1, n2->i2, n2->i3);
	*/
	
	creader_t *c = alloc_creader();
	cr_read_file(c, "config_reader/test.conf");
	cr_iterator_t iter;
	printf("Test1\n");
	for (cr_line_t *l = cr_init_iter(&iter, c, "SERVER_PARAMS", false); l != NULL; l = cr_iter_next(&iter)){
		printf("%s %s %s\n", l->group, l->key, l->value);
	}
	printf("Test2\n");
	for (cr_line_t *l = cr_init_iter(&iter, c, NULL, true); l != NULL; l = cr_iter_next(&iter)){
		printf("%s %s %s\n", l->group, l->key, l->value);
	}
	printf("Test3\n");
	for (cr_line_t *l = cr_init_iter(&iter, c, NULL, false); l != NULL; l = cr_iter_next(&iter)){
		printf("%s %s %s\n", l->group, l->key, l->value);
	}
	/*
	cr_line_t *lines = NULL;
	int count = cr_get_lines(c, "SERVER_PARAMS", &lines);
	printf("%i %p\n", count, lines); 
	for (int i = 0; i < count; i++){
		cr_line_t *line = lines + i;
		printf("%s %s %s\n", line->group, line->key, line->value);
	}*/
	printf("Test4\n");
	printf("%s\n", cr_get_value(c, "SERVER_PARAMS", "family"));
	printf("Test5\n");
	printf("%i\n", cr_get_count(c, "SERVER_PARAMS", true));
	
	free_creader(c);
	
	return 0;
}