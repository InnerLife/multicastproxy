/** @file config_reader.h
 *  @brief Read config file in a group-key-value format
 *
 * [GROUP1]
 * key value
 * key value#comment
 * [GROUP2]
 * key value
 *
 * if key or value is missing, line will be silently ignored
 */
#ifndef H_CONFIG_READER
#define H_CONFIG_READER

#include "../log/log.h"
#include <stdbool.h>
#include <stdint.h>

#define COMMENT_BEGIN '#'

/**
 * @brief Parsed config line
 */
typedef struct cr_line {
	char *group; /**< config line group */
	char *key; /**< config line key */
	char *value; /**< config line value */
} cr_line_t;

typedef struct creader {
	char *file; /**< char array read from config file */
	cr_line_t *lines; /**< parsed config lines */
	uint32_t lines_count; /**< count of config lines */
} creader_t;

/**
 * @brief Config lines iterator
 */
typedef struct cr_iterator {
	creader_t *reader; /**< config reader instance */
	cr_line_t *line; /**< current line */
	char* group; /**< iterate only over specified group, group filter */
	bool uniq_group; /**< iterate only over unique group names, move iterator to a line with next unique group name, ignore group filter if set */
} cr_iterator_t;

/**
 * @brief Initialize already allocated config reader
 * 
 * @param reader Config reader
 * @return config reader if success, otherwise - NULL
 */
creader_t* init_creader(creader_t *reader);
/**
 * @brief Allocate and initialize config reader
 * 
 * @return config reader if success, otherwise - NULL
 */
creader_t* alloc_creader();
/**
 * @brief Free config reader internal data structures
 * 
 * @param reader Config reader
 */
void free_creader_data(creader_t *reader);
/**
 * @brief Free config reader internal data structures and reader ptr itself
 * 
 * @param reader Config reader
 */
void free_creader(creader_t *reader);

/**
 * @brief Read and parse file
 * 
 * @param reader Config reader
 * @param file_name File name
 * @return Config reader if success, otherwise - NULL
 */
creader_t* cr_read_file(creader_t *reader, char *file_name);

/**
 * @brief Move iterator to next line
 * 
 * @param iter config reader iterator
 * @return line if found, otherwise - NULL
 */
cr_line_t* cr_iter_next(cr_iterator_t *iter);
/**
 * @brief Initialize iterator
 * 
 * @param iter config reader iterator
 * @param reader config reader
 * @param group group filter, iterate only over specified group
 * @param uniq_group iterate only over lines with uniq group names, ignore group filter if set
 * @return line if found, otherwise - NULL
 */
cr_line_t* cr_init_iter(cr_iterator_t *iter, creader_t *reader, char* group, bool uniq_group);

/**
 * @brief Get count of lines using config reader iterator
 * 
 * @param reader config reader
 * @param group see above
 * @param uniq_group see above
 * @return lines count
 */
uint32_t cr_get_count(creader_t *reader, char* group, bool uniq_group);
/**
 * @brief Get param value for specified group and key
 * 
 * @param reader config reader
 * @param group group
 * @param key key
 * @return value if found, otherwise - NULL
 */
char* cr_get_value(creader_t *reader, char *group, char *key);

#endif

#ifndef CR_LOG_LEVEL
#define CR_LOG_LEVEL C_LOG_DEFAULT
#endif