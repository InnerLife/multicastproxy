#include "config_reader.h"

#include <stdlib.h>
#include <ctype.h>

//read whole file to char array and return it
static char* load_file(char *file_name){
	FILE *file;
	if((file = fopen(file_name, "r")) == NULL){
		LOG_ERROR(CR_LOG_LEVEL, "Config file %s open error.", file_name);
		return NULL;
	}

	fseek(file, 0, SEEK_END);
	long int size = ftell(file);
	fseek(file, 0, SEEK_SET);
	LOG_DEBUG(CR_LOG_LEVEL, "Config file %s opened. Size: %li", file_name, size);

	char *str_file = malloc(size + 1);
	if (str_file == NULL){
		LOG_ERROR(CR_LOG_LEVEL, "Can not allocate string for file %s.", file_name);
		free(str_file);
		return NULL;
	}	
	fread(str_file, size, 1, file);
	fclose(file);
	str_file[size] = NUL;
	return str_file;
}

static cr_line_t* parse_word(creader_t *reader, char *word, uint32_t word_num){
	//LOG_DEBUG(CR_LOG_LEVEL, "W(%lu, %u): %s", strlen(word), word_num, word);
	cr_line_t *line = &reader->lines[reader->lines_count];
	//if it's a first word in line, check if it's a group or a key
	if (word_num == 0){
		if (word[0] == '[' && word[strlen(word) - 1] == ']'){
			//if it's a group add it to appending line
			word[strlen(word) - 1] = NUL;
			line->group = word + 1;
		} else {
			//if it's a key and group name is empty, try to get it from the previous line
			if (line->group == NULL){
				if (reader->lines_count == 0){
					return NULL;
				}
				line->group = reader->lines[reader->lines_count - 1].group;
			}
			line->key = word;
		}
	} 
	//second word can be only value in key-value pair, copy it and return a command to allocate an appending new line
	else if (word_num == 1) {
		if (line->key == NULL){
			return NULL;
		}
		line->value = word;
		reader->lines_count++;
		LOG_DEBUG(CR_LOG_LEVEL, "Line readed [%s,%s,%s]. %p", line->group, line->key, line->value, reader);
		return line;
	}
	return NULL;
}

//allocate a new line to reader, after previously line was successfully parsed
//there always +1 line, last line is appending line and using to store data of currently parsing line, which may or may not be successfully parsed
static cr_line_t* add_line(creader_t *reader){
	cr_line_t *new_lines = realloc(reader->lines, (reader->lines_count + 1) * sizeof(*reader->lines));
	if (new_lines == NULL){
		LOG_ERROR(CR_LOG_LEVEL, "Error reallocating reader lines. %p", reader);
		return NULL;
	}
	reader->lines = new_lines;
	memset(reader->lines + reader->lines_count, 0, sizeof(*reader->lines));
	LOG_DEBUG(CR_LOG_LEVEL, "Lines reallocated. %p", reader);
	return reader->lines;
}

static cr_line_t* parse_line(creader_t *reader, char *line){
	//NPRINTF("L(%lu): %s", strlen(line), line);
	char *word_begin = NULL;
	char *ptr = line - 1;
	uint32_t word_num = 0;
	//iterate through line splitting it to words
	while(*(++ptr) != NUL){
		//mark first letter as word beginning
		if (word_begin == NULL && isspace(*ptr) == 0){
			word_begin = ptr;
		}
		//mark space as word end
		if (word_begin != NULL && isspace(*ptr) != 0){
			*ptr = NUL;
		}
		//when found a word - parce it, and, if it was last word - add a line
		if ((*ptr == NUL || *(ptr + 1) == NUL) && word_begin != NULL){
			if (parse_word(reader, word_begin, word_num) != NULL){
				return add_line(reader);
			}
			word_begin = NULL;
			word_num++;
		}
	}
	return reader->lines;
}

static creader_t* parse_file(creader_t *reader){
	char *line_begin = reader->file;
	char *ptr = reader->file - 1;
	//iterate through file trying to find line end
	while(*(++ptr) != NUL){
		//replacing comment and windows \r with NUL and continue trying to find a real line end
		if (*ptr == COMMENT_BEGIN || *ptr == '\r'){
			*ptr = NUL;
			continue;
		}
		//when real end is found - NUL it
		if (*ptr == '\n') {
			*ptr = NUL;
		}
		//when line end is found, parsing a line first, second continue with next line
		if (*ptr == NUL || *(ptr + 1)/*there may be no newline at the end of the file*/ == NUL){
			if (parse_line(reader, line_begin) == NULL){
				return NULL;
			}
			line_begin = ptr + 1;
		}
	}
	return reader;
}

creader_t* init_creader(creader_t *reader){
	reader->lines = calloc(1, sizeof(*reader->lines));
	if (reader->lines == NULL){
		LOG_ERROR(CR_LOG_LEVEL, "Error allocating config reader lines. %p", reader);
		return NULL;
	}
	reader->file = NULL;
	reader->lines_count = 0;
	LOG_DEBUG(CR_LOG_LEVEL, "Config reader initialized. %p", reader);
	return reader;
}

creader_t* alloc_creader(){
	creader_t *reader = malloc(sizeof(*reader));
	if (reader == NULL){
		LOG_ERROR(CR_LOG_LEVEL, "Error allocating config reader.");
	}
	if (init_creader(reader) == NULL){
		free(reader);
		return reader;
	}	
	LOG_DEBUG(CR_LOG_LEVEL, "Config reader allocated. %p", reader);
	return reader;
}

void free_creader_data(creader_t *reader){
	free(reader->file);
	free(reader->lines);
	LOG_DEBUG(CR_LOG_LEVEL, "Config reader data freed. %p", reader);
}

void free_creader(creader_t *reader){
	free_creader_data(reader);
	free(reader);
	LOG_DEBUG(CR_LOG_LEVEL, "Config reader freed. %p", reader);
}

creader_t* cr_read_file(creader_t *reader, char *file_name){
	LOG_DEBUG(CR_LOG_LEVEL, "Config reader. Reading file %s... %p", file_name, reader);
	reader->file = load_file(file_name);
	if (reader->file == NULL){
		LOG_ERROR(CR_LOG_LEVEL, "Config reader. Error reading file. %p", reader);
		return NULL;
	}
	if (parse_file(reader) == NULL){
		LOG_ERROR(CR_LOG_LEVEL, "Config reader. Error parsing file. %p", reader);
		return NULL;
	}
	LOG_DEBUG(CR_LOG_LEVEL, "File %s read and parsed. %p", file_name, reader);
	return reader;
}

cr_line_t* cr_iter_next(cr_iterator_t *iter){
	if (iter->line == NULL){
		iter->line = iter->reader->lines;
	} else {
		iter->line++;
	}
	if (iter->line >= iter->reader->lines + iter->reader->lines_count){
		return NULL;
	}
	if (iter->uniq_group){
		if (iter->group == NULL || strcmp(iter->group, iter->line->group) != 0){
			iter->group = iter->line->group;
			return iter->line;
		}
		return cr_iter_next(iter);
	} else if (iter->group != NULL) {
		if (strcmp(iter->group, iter->line->group) == 0){
			return iter->line;
		}
		return cr_iter_next(iter);
	}
	return iter->line;
}

cr_line_t* cr_init_iter(cr_iterator_t *iter, creader_t *reader, char* group, bool uniq_group){
	iter->reader = reader;
	iter->line = NULL;
	iter->group = group;
	iter->uniq_group = uniq_group;
	if (uniq_group){
		iter->group = NULL;
	}
	return cr_iter_next(iter);
}

uint32_t cr_get_count(creader_t *reader, char* group, bool uniq_group){
	uint32_t count = 0;
	cr_iterator_t iter;
	for (cr_line_t *l = cr_init_iter(&iter, reader, group, uniq_group); l != NULL; l = cr_iter_next(&iter)){
		count++;
	}
	return count;
}

char* cr_get_value(creader_t *reader, char *group, char *key){
	cr_iterator_t iter;
	for (cr_line_t *l = cr_init_iter(&iter, reader, group, false); l != NULL; l = cr_iter_next(&iter)){
		if (strcmp(l->key, key) == 0){
			LOG_DEBUG(CR_LOG_LEVEL, "Value found [%s,%s,%s]. %p", group, key, l->value, reader);
			return l->value;
		}
	}
	LOG_DEBUG(CR_LOG_LEVEL, "Value not found [%s,%s]. %p", group, key, reader);
	return NULL;
}

