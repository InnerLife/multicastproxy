/** @file mps_worker.h
 *  @brief mp_server worker. Creates it's own epoll fd and connection listener.
 */
#ifndef H_MPS_WORKER
#define H_MPS_WORKER

#include "log/log.h"
#include "mp_server.h"

#include <stdbool.h>

typedef struct mps_worker {
	void *conn_server; /**< connection listener, client connections holder */
	int epfd; /**< epoll fd */
	uint32_t epoll_max_events; /**< epoll_wait max events */
	uint32_t epoll_timeout; /**< epoll_wait timeout */
	time_t last_cleaned; /**< last connections cleaning datetime */
	uint32_t backend_idle_max; /**< max time backend connection send buffer can be idle until cleaned */
	uint32_t client_idle_max; /**< max time client and backend connection can be idle until cleaned */
	uint32_t idle_cleaner_sleep; /**< call cleaner not more often than idle_cleaner_sleep */
	struct epoll_event *events; /**< array of epoll_max_events events */
} mps_worker_t;

/**
 * @brief Initialize already allocated worker instance
 * 
 * @param worker allocated mps_worker instance
 * @param 'multiple params' - see above
 * @return worker if success, otherwise - NULL
 */
mps_worker_t* init_mps_worker(mps_worker_t *worker, uint32_t buffer_size, uint32_t max_buffers, int type, int family, short port, int backlog, server_meta_t *smeta, uint32_t clients_count, 
							uint32_t epoll_max_events, uint32_t epoll_timeout, uint32_t backend_idle_max, uint32_t client_idle_max, uint32_t idle_cleaner_sleep);					
/**
 * @brief Allocate and initialize already allocated worker instance
 * 
 * @param 'multiple params' - see above
 * @return worker if success, otherwise - NULL
 */
mps_worker_t* alloc_mps_worker(uint32_t buffer_size, uint32_t max_buffers, int type, int family, short port, int backlog, server_meta_t *smeta, uint32_t clients_count, 
								uint32_t epoll_max_events, uint32_t epoll_timeout, uint32_t backend_idle_max, uint32_t client_idle_max, uint32_t idle_cleaner_sleep);
/**
 * @brief Free worker instance internal data structures
 * 
 * @param worker mps_worker instance
 */
void free_mps_worker_data(mps_worker_t *worker);
/**
 * @brief Free worker instance internal data structures and worker ptr itself
 * 
 * @param worker mps_worker instance
 */
void free_mps_worker(mps_worker_t *worker);

/**
 * @brief Poll epfd once and process events, run cleaner if necessary
 * 
 * @param worker mps_worker instance
 * @return worker if success, otherwise - NULL
 */
mps_worker_t* mps_worker_poll(mps_worker_t *worker);

#endif

#ifndef MPS_W_LOG_LEVEL
#define MPS_W_LOG_LEVEL C_LOG_DEFAULT
#endif
