#include "linked_list.h"
#include "linked_list.c"

#include <stdlib.h>

typedef struct test {
	uint64_t i1;
	uint64_t i2;
	uint64_t i3;
} test_t;

int main(){
	llist_t *l = alloc_llist(sizeof(test_t), 5);
	
	printf("Test1\n");
	test_t *t1 = llist_take_prealloc(l, 0);	
	t1->i1 = 1; t1->i2 = 2; t1->i3 = 3;
	printf("%p %lu %lu %lu\n", t1, t1->i1, t1->i2, t1->i3);
	t1 = llist_get_first(l);
	printf("%p %lu %lu %lu\n", t1, t1->i1, t1->i2, t1->i3);
	t1 = llist_get_last(l);
	printf("%p %lu %lu %lu\n", t1, t1->i1, t1->i2, t1->i3);
	llist_release(l, t1);
	
	printf("Test2\n");
	printf("%d\n", l->size);
	for (int i = 0; i < 10; i++){ 
		test_t *t = llist_take(l);
		t->i1 = i + 1; t->i2 = i + 2; t->i3 = i + 3;
	}
	printf("%d\n", l->size);
	llist_iterator_t iter;
	for (test_t *t = llist_iter_init(&iter, l); t != NULL; t = llist_iter_next(&iter)){ 
		printf("%p %lu %lu %lu\n", iter.cur, t->i1, t->i2, t->i3);
		llist_release(l, t);
	}
	
	printf("Test3\n");
	printf("%d\n", l->size);
	//int j = 0;
	for (int i = 0; i < 10; i++){ 
		test_t *t = llist_take(l);
		t->i1 = i + 1; t->i2 = i + 2; t->i3 = i + 3;
		//j = i;
	}
	printf("%d\n", l->size);
	for (test_t *t = llist_iter_init(&iter, l); t != NULL; t = llist_iter_next(&iter)){ 
		printf("%p %lu %lu %lu\n", iter.cur, t->i1, t->i2, t->i3);
		//llist_release(l, t);
	}
	
	test_t *t2 = llist_get_first(l);	
	printf("%p %lu %lu %lu\n", t2, t2->i1, t2->i2, t2->i3);
	test_t *t3 = llist_get_last(l);	
	printf("%p %lu %lu %lu\n", t3, t3->i1, t3->i2, t3->i3);
	
	printf("%p\n", llist_get_prealloc(l, 9));
	printf("%p\n", llist_take_prealloc(l, 3));
	
	test_t *t4 = llist_get_prealloc(l, 3);	
	printf("%p %lu %lu %lu\n", t4, t4->i1, t4->i2, t4->i3);
	
	free_llist(l);
	
	return 0;
}