/** @file linked_list.h
 *  @brief Linked list with fixed nodes preallocation. Fast node removing while iterating using iterator. Preallocated nodes can be fast accessed using index.
 * Allocate once, use forever - one malloc if you know exact number of nodes needed, one extra malloc for each non-preallocated node.
 * Preallocated nodes memory area placed right after list memory area - no need of extra ptr to nodes area, saves sizeof(void*) memory for each list.
 * ++++++++++++++++++++++++++++++++++++++++++
 * +                                        +
 * +           llist_t memory area          +
 * +                                        +
 * ++++++++++++++++++++++++++++++++++++++++++
 * +                                        +
 * +   prealloc nodes area (llist_t + 1)    +
 * +                                        +
 * ++++++++++++++++++++++++++++++++++++++++++
 * ++                                      ++ 
 * ++          llist_node_t(node0)         ++
 * ++                                      ++
 * ++++++++++++++++++++++++++++++++++++++++++
 * +++                                    +++
 * +++           data(node0 + 1)          +++
 * +++                                    +++
 * ++++++++++++++++++++++++++++++++++++++++++
 * ++                                      ++ 
 * ++          llist_node_t(nodeN)         ++
 * ++                                      ++
 * ++++++++++++++++++++++++++++++++++++++++++
 * +++                                    +++
 * +++           data(nodeN + 1)          +++
 * +++                                    +++
 * ++++++++++++++++++++++++++++++++++++++++++
 */
#ifndef H_LINKED_LIST
#define H_LINKED_LIST

#include "../log/log.h"
#include <stdint.h>

/**
 * @brief Get llist node size by data size.
 */
#define LL_NODE_SIZE(data_size) (sizeof(llist_node_t) + data_size)
/**
 * @brief Get llist prealloc area size by data size and a number of nodes to preallocate.
 */
#define LL_PREALLOC_SIZE(data_size, prealloc_size) (LL_NODE_SIZE(data_size) * prealloc_size)
/**
 * @brief Get size of a whole list including preallocated area by data size and a number of nodes to preallocate..
 */
#define LL_SIZE(data_size, prealloc_size) (sizeof(llist_t) + LL_PREALLOC_SIZE(data_size, prealloc_size))
/**
 * @brief Get ptr to data using node ptr.
 */
#define LL_NODE_TO_DATA(node) ((void*)(node + 1))
/**
 * @brief Get ptr to node using data ptr.
 */
#define LL_DATA_TO_NODE(data) (((llist_node_t*)data) - 1)

typedef struct llist_node {
	struct llist_node *next; /**< ptr to next node */
	struct llist_node *prev; /**< ptr to prev node */
} llist_node_t;

/**
 * @brief Fixed size list.
 */
typedef struct llist {
	llist_node_t *first; /**< ptr to first taken node */
	llist_node_t *last; /**< ptr to last taken node */
	llist_node_t *free; /**< ptr to first free node */
	uint32_t size; /**< count of nodes taken */
	uint32_t data_size; /**< size of data */
	uint32_t prealloc_size; /**< count of preallocated nodes */
} llist_t;

typedef struct llist_iterator {
	llist_node_t *cur; /**< current node */
	llist_node_t *next; /**< next node to iterate, cached from cur node to support removing */
} llist_iterator_t;

/**
 * @brief Initialize already allocated list. List should be allocated correspond to prealloc_size.
 * 
 * @param list list
 * @param data_size data size
 * @param prealloc_size count of preallocated nodes
 */
void init_llist(llist_t *list, uint32_t data_size, uint32_t prealloc_size);
/**
 * @brief Allocate and initialize list. List should be allocated correspond to prealloc_size.
 * 
 * @param list list
 * @param data_size data size
 * @param prealloc_size count of preallocated nodes
 * @return list if success, otherwise - NULL
 */
llist_t* alloc_llist(uint32_t data_size, uint32_t prealloc_size);
/**
 * @brief Free list internal data structures.
 * 
 * @param list list
 */
void free_llist_data(llist_t *list);
/**
 * @brief Free list internal data structures and list ptr itself.
 * 
 * @param list list
 */
void free_llist(llist_t *list);

/**
 * @brief Take free node data from list. Alloc new node if needed.
 * 
 * @param list list
 * @return ptr to data if success, NULL - allocation error
 */
void* llist_take(llist_t *list);
/**
 * @brief Take free node data from list preallocated area by index.
 * 
 * @param list list
 * @return ptr to data if success, NULL - index is out of range or node is already taken
 */
void* llist_take_prealloc(llist_t *list, uint32_t index);
/**
 * @brief Release taken node. Given ptr to data is not checking.
 * 
 * @param list list
 */
void llist_release(llist_t *list, void *data);
/**
 * @brief Move node to tail. Given ptr to data is not checking.
 * 
 * @param list list
 */
void llist_to_last(llist_t *list, void *data);

/**
 * @brief Get the oldest taken node data.
 * 
 * @param list list
 * @return ptr to data if success, NULL - no node was taken
 */
void* llist_get_first(llist_t *list);
/**
 * @brief Get the earlier taken node data.
 * 
 * @param list list
 * @return ptr to data if success, NULL - no node was taken
 */
void* llist_get_last(llist_t *list);
/**
 * @brief Get node data from list preallocated area by index.
 * 
 * @param list list
 * @return ptr to data if success, NULL - index is out of range or node is not taken
 */
void* llist_get_prealloc(llist_t *list, uint32_t index);

/**
 * @brief Get the next iterator node data.
 * 
 * @param iter iterator
 * @return ptr to data if success, NULL - no nodes left
 */
void* llist_iter_next(llist_iterator_t *iter);
/**
 * @brief initialize iterator and get first node data.
 * 
 * @param iter iterator
 * @param list list
 * @return ptr to data if success, NULL - no node was taken
 */
void* llist_iter_init(llist_iterator_t *iter, llist_t *list);

#endif

#ifndef LL_LOG_LEVEL
#define LL_LOG_LEVEL C_LOG_DEFAULT
#endif
