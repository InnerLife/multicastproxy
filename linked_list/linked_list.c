#include "linked_list.h"

#include <stdlib.h>

void init_llist(llist_t *list, uint32_t data_size, uint32_t prealloc_size){
	list->first = NULL;
	list->last = NULL;
	list->size = 0;
	list->data_size = data_size;
	if (prealloc_size <= 0){
		list->prealloc_size = 0;
		list->free = NULL;
		return;
	}
	LOG_DEBUG(LL_LOG_LEVEL, "Initializing preallocated area... %p %ld bytes", list, LL_PREALLOC_SIZE(data_size, prealloc_size));
	list->prealloc_size = prealloc_size;
	list->free = (llist_node_t*)(list + 1);
	llist_node_t *prev_node = NULL;
	llist_node_t *cur_node = list->free;
	for (uint32_t i = 0; i < prealloc_size; i++){
		llist_node_t *next_node = (llist_node_t*)((char*)cur_node + LL_NODE_SIZE(list->data_size));
		cur_node->next = next_node;
		cur_node->prev = (void*)list;//mark node as not taken; list of free preallocated nodes is unidirectional, so we can use prev node ptr in a dirty way to save memory
		//LOG_DEBUG(LL_LOG_LEVEL, "Node initialized... %p %p", list, cur_node);
		prev_node = cur_node;
		cur_node = next_node;
	}	
	prev_node->next = NULL;
}

llist_t* alloc_llist(uint32_t data_size, uint32_t prealloc_size){
	LOG_DEBUG(LL_LOG_LEVEL, "Allocating linked list...");
	llist_t *list = malloc(LL_SIZE(data_size, prealloc_size));
	if (list == NULL){
		LOG_ERROR(LL_LOG_LEVEL, "Error allocating memory for linked list.");
		return NULL;
	}
	LOG_DEBUG(LL_LOG_LEVEL, "Linked list allocated. %p", list);
	init_llist(list, data_size, prealloc_size);
	return list;
}

void free_llist_data(llist_t *list){
	llist_iterator_t iter;
	for (void *d = llist_iter_init(&iter, list); d != NULL; d = llist_iter_next(&iter)){
		llist_release(list, d);
	}
	LOG_DEBUG(LL_LOG_LEVEL, "List data freed. %p", list);
}

void free_llist(llist_t *list){
	free_llist_data(list);
	free(list);
	LOG_DEBUG(LL_LOG_LEVEL, "List freed. %p", list);
}

static void* take_node(llist_t *list, llist_node_t *node){
	node->prev = list->last;
	node->next = NULL;
	if (list->last != NULL){
		list->last->next = node;
	} else {
		list->first = node;
	}	
	list->last = node;
	list->size++;
	return LL_NODE_TO_DATA(node);
}

void* llist_take(llist_t *list){
	llist_node_t *node = list->free;
	if (node == NULL){
		node = malloc(LL_NODE_SIZE(list->data_size));
		if (node == NULL){
			LOG_ERROR(LL_LOG_LEVEL, "Error allocating linked list node. %p", list);
			return NULL;
		}
		LOG_DEBUG(LL_LOG_LEVEL, "Node taken. %p %p", list, node);
	} else {
		list->free = node->next;
		LOG_DEBUG(LL_LOG_LEVEL, "Node taken from preallocated area. %p %p", list, node);
	}
	return take_node(list, node);
}

static void* node_by_index(llist_t *list, uint32_t index){
	if (index >= list->prealloc_size){
		LOG_ERROR(LL_LOG_LEVEL, "Can not get node by index out of preallocated area. %p", list);
		return NULL;
	}
	llist_node_t *node = (llist_node_t*)((char*)(list + 1) + LL_NODE_SIZE(list->data_size) * index);
	return node;
}

void* llist_take_prealloc(llist_t *list, uint32_t index){
	llist_node_t *node = node_by_index(list, index);
	if (node == NULL){
		return NULL;
	}
	if (node->prev != (void*)list){
		LOG_ERROR(LL_LOG_LEVEL, "Node is already taken. %p %p", list, node);
		return NULL;
	}
	LOG_DEBUG(LL_LOG_LEVEL, "Node taken by index. %p %p %d", list, node, index);
	if (node == list->free){
		list->free = node->next;
	}
	return take_node(list, node);
}

void llist_release(llist_t *list, void *data){
	llist_node_t *node = LL_DATA_TO_NODE(data);
	LOG_DEBUG(LL_LOG_LEVEL, "Releasing node. %p %p", list, node);
	if (node->prev == NULL){
		list->first = node->next;
	} else {
		node->prev->next = node->next;
	}
	if (node->next == NULL){
		list->last = node->prev;
	} else {
		node->next->prev = node->prev;
	}
	
	if (list->prealloc_size > 0 && (llist_t*)node >= (list + 1) && (char*)node < ((char*)(list + 1) + LL_PREALLOC_SIZE(list->data_size, list->prealloc_size))){
		node->next = list->free;
		node->prev = (void*)list;//mark node as not not taken
		list->free = node;
		LOG_DEBUG(LL_LOG_LEVEL, "Node from preallocated area released. %p %p", list, node);
	} else {		
		LOG_DEBUG(LL_LOG_LEVEL, "Node released. %p %p", list, node);
		free(node);
	}
	list->size--;
}

void llist_to_last(llist_t *list, void *data){
	llist_node_t *node = LL_DATA_TO_NODE(data);
	if (node == list->last){
		return;
	}
	if (node->prev == NULL){
		list->first = node->next;
	} else {
		node->prev->next = node->next;
	}
	node->prev = list->last;
	node->next = NULL;
	list->last->next = node;
	list->last = node;
}

void* llist_get_first(llist_t *list){
	if (list->first == NULL){
		return NULL;
	}
	LOG_DEBUG(LL_LOG_LEVEL, "First node gotten. %p %p", list, list->first);
	return LL_NODE_TO_DATA(list->first);
}

void* llist_get_last(llist_t *list){
	if (list->last == NULL){
		return NULL;
	}
	LOG_DEBUG(LL_LOG_LEVEL, "Last node gotten. %p %p", list, list->last);
	return LL_NODE_TO_DATA(list->last);
}

void* llist_get_prealloc(llist_t *list, uint32_t index){
	llist_node_t *node = node_by_index(list, index);
	if (node == NULL){
		return NULL;
	}
	if (node->prev == (void*)list){
		LOG_ERROR(LL_LOG_LEVEL, "Node is not taken. %p %p", list, node);
		return NULL;
	}
	LOG_DEBUG(LL_LOG_LEVEL, "Node gotten by index. %p %p %d", list, node, index);
	return LL_NODE_TO_DATA(node);
}

void* llist_iter_next(llist_iterator_t *iter){
	if (iter->next == NULL){
		return NULL;
	}
	iter->cur = iter->next;
	iter->next = iter->cur->next;
	return LL_NODE_TO_DATA(iter->cur);
}

void* llist_iter_init(llist_iterator_t *iter, llist_t *list){
	iter->cur = NULL;
	iter->next = list->first;
	return llist_iter_next(iter);
}