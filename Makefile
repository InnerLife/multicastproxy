CC=gcc
override CFLAGS += -O3 -pthread -Wall

SOURCES := $(filter-out $(wildcard */test*), $(wildcard *.c) $(wildcard */*.c))
TEST_CONFIG ?= "mp_config/test.conf"

.PHONY: compile compile_run debug test_submodule clean

#just compile release version, default
compile:
	$(CC) $(CFLAGS) $(SOURCES) ${ARGS} -o mp_server

#compile release and run
test:
	$(MAKE) ARGS="${ARGS} -pg" compile
	./mp_server $(TEST_CONFIG)

#compile with debug messages enabled and run in valgrind
debug:
	$(MAKE) ARGS="${ARGS} -ggdb3 -pg -D C_LOG_DEFAULT=C_LOG_DEBUG -D LL_LOG_LEVEL=C_LOG_INFO -D MPC_LOG_LEVEL=C_LOG_INFO -D CR_LOG_LEVEL=C_LOG_INFO" compile
	valgrind --leak-check=full ./mp_server $(TEST_CONFIG)

#compile submodule with debug messages enabled and run in valgrind
%.o:
	$(MAKE) MODULE="$(basename $@)" test_submodule

#^above
test_submodule:
	$(CC) $(CFLAGS) -ggdb3 -D C_LOG_DEFAULT=C_LOG_DEBUG ${MODULE}/test.c -o ${MODULE}/test.o
	valgrind --leak-check=full ${MODULE}/test.o

#clean all binaries
clean:
	@rm -f */test.o
	@rm -f mp_server
