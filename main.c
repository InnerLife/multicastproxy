#include "log/log.h"
#include "config_reader/config_reader.h"
#include "mp_server.h"

#include <stdio.h>
#include <errno.h>
#include <string.h>

#include <stdlib.h>
#include <signal.h>

mp_server_t mpserver;

static void signal_callback(int signum){
	if (!mpserver.stopped) {
		LOG_INFO(C_LOG_DEBUG, "Stopping server...");
		mpserver.stopped = true;
	}
}

static void* start_threads(){
	pthread_t threads[mpserver.params.workers + 1];
	for (int i = 0; i < mpserver.params.workers; i++){
		pthread_t *thread = &threads[i];
		if (pthread_create(thread, NULL, mps_worker_thread, &mpserver) != 0){
			LOG_ERROR(C_LOG_DEBUG, "Can not create server. Error starting worker thread.");
			return NULL;
		}
	}
	pthread_t *reload_thread = &threads[mpserver.params.workers];
	if (pthread_create(reload_thread, NULL, mps_reload_backends_thread, &mpserver) != 0){
		LOG_ERROR(C_LOG_DEBUG, "Can not create server. Error starting backends reloading thread.");
		return NULL;
	}
	for (int i = 0; i < mpserver.params.workers + 1; i++){
		pthread_t thread = threads[i];
		pthread_join(thread, NULL);
	}
	return &mpserver;
}

int main(int argc, char **argv){
	if (argc <= 1){
		LOG_ERROR(C_LOG_DEBUG, "Can not start server. Arguments can not be empty.");
		return EXIT_FAILURE;
	}
	creader_t reader;
	if (init_creader(&reader) == NULL){
		LOG_ERROR(C_LOG_DEBUG, "Can not start server. Error allocating config reader.");
		return EXIT_FAILURE;
	}
	if (cr_read_file(&reader, argv[1]) == NULL){
		LOG_ERROR(C_LOG_DEBUG, "Can not start server. Error reding config file.");
		free_creader_data(&reader);
		return EXIT_FAILURE;
	}
	if (init_mpserver(&mpserver, &reader) == NULL){
		LOG_ERROR(C_LOG_DEBUG, "Can not start server. Error creating server.");
		free_creader_data(&reader);
		return EXIT_FAILURE;
	}
	free_creader_data(&reader);
	
	signal(SIGINT, signal_callback);
	signal(SIGTERM, signal_callback);
	
	void *res = start_threads();
	free_mpserver_data(&mpserver);
	if (res == NULL){
		return EXIT_FAILURE;
	}
	return EXIT_SUCCESS;
}
/*
gcc -D LL_LOG_LEVEL=C_LOG_INFO -D MPC_LOG_LEVEL=C_LOG_INFO -D CR_LOG_LEVEL=C_LOG_INFO -ggdb3 -Wall main.c mp_server.c mps_worker.c server_meta/server_meta.c config_reader/config_reader.c mp_config/mp_config.c linked_list/linked_list.c uni_addr/uni_addr.c backend/backend.c connection/connection.c connection/connection_server.c connection/connection_client.c connection/connection_backend.c -pthread -o test.o && valgrind --leak-check=full ./test.o mp_config/test.conf

gdb -ex run --args test.o mp_config/test.conf

gcc -D LL_LOG_LEVEL=C_LOG_INFO -D MPC_LOG_LEVEL=C_LOG_INFO -D CR_LOG_LEVEL=C_LOG_INFO -D MPS_LOG_LEVEL=C_LOG_INFO -D MPS_W_LOG_LEVEL=C_LOG_INFO -D CONN_LOG_LEVEL=C_LOG_INFO -D CONN_C_LOG_LEVEL=C_LOG_INFO -Wall main.c mp_server.c mps_worker.c server_meta/server_meta.c config_reader/config_reader.c mp_config/mp_config.c linked_list/linked_list.c uni_addr/uni_addr.c backend/backend.c connection/connection.c connection/connection_server.c connection/connection_client.c connection/connection_backend.c -pthread -o test.o && ./test.o mp_config/test.conf
*/